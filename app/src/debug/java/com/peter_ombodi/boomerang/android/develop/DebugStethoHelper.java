package com.peter_ombodi.boomerang.android.develop;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.peter_ombodi.boomerang.android.develop.IStethoConfigure;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class DebugStethoHelper implements IStethoConfigure {
    @Override
    public void init(Application application) {
        Stetho.initializeWithDefaults(application);
    }

    @Override
    public void configureInterceptor(OkHttpClient.Builder httpClient) {
        //For see retrofit requests in Chrome inspect
        httpClient.addNetworkInterceptor(new StethoInterceptor());

        //For see retrofit logs in Logcat
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addNetworkInterceptor(logging);
    }
}
