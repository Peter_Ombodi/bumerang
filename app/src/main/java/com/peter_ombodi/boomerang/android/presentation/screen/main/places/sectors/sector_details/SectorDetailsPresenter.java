package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 09.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public final class SectorDetailsPresenter extends BasePresenter implements SectorDetailsContract.SectorPresenter {

    private SectorDetailsContract.SectorView view;
    private SectorDetailsContract.SectorModel model;

    public SectorDetailsPresenter(SectorDetailsContract.SectorView view, SectorDetailsContract.SectorModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void onSaveClick(PlaceItem item) {
        compositeSubscriptions.add(
                model.saveSector(item)
                        .subscribe(newId -> {
                            item.setId(newId);
                            view.onSectorSaved(item);
                        }, Throwable::printStackTrace));
    }

    @Override
    public void onDeleteClick(long sectorId) {
        compositeSubscriptions.add(
                model.deleteSector(sectorId)
                        .subscribe(cnt -> {
                            if (cnt>0)
                                view.onSectorDeleted();
                        }, Throwable::printStackTrace));
    }

    @Override
    public void getCategories() {
        view.showProgressMain();
        compositeSubscriptions.add(
                model.getCategories().subscribe(categories -> {
                            view.setCategories(categories);
                            view.hideProgress();
                        },
                        throwable -> {
                            view.hideProgress();
                            throwable.printStackTrace();
                        }));
    }
}