package com.peter_ombodi.boomerang.android.presentation.screen.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.peter_ombodi.boomerang.android.BuildConfig;
import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.setting.SettingsManager;
import com.peter_ombodi.boomerang.android.presentation.BoomerangApp_;
import com.peter_ombodi.boomerang.android.presentation.base.MVPActivity;
import com.peter_ombodi.boomerang.android.presentation.screen.main.home.HomeFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.PlacesFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.settings.SettingsFragment_;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends MVPActivity<MainContract.MainPresenter> implements MainContract.MainView, NavigationView.OnNavigationItemSelectedListener {

    private static final int PERMISSIONS_SETTINGS_RESULT_CODE = 501;
    private static final String PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    @Bean
    MainInteractor mainInteractor;
    @Bean
    SettingsManager settingsManager;

    @ViewById
    protected ProgressBar pbPagination;
    @ViewById
    protected FrameLayout flFragmentContent_AD;
    @ViewById(R.id.toolbar_AD)
    protected Toolbar toolbar;
    @ViewById(R.id.drawerLayout_AD)
    protected DrawerLayout drawerLayout;
    @ViewById(R.id.nvDrawer_AD)
    protected NavigationView navigationView;

    private ActionBarDrawerToggle drawerToggle;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private RxPermissions rxPermissions;
    protected ImageView ivAvatar_VDH;
    protected TextView tvName_VDH;


    @AfterInject
    @Override
    public void initPresenter() {
        new MainPresenter(this, mainInteractor);
    }

    @AfterInject
    void initLocale() {
        BoomerangApp_.getInstance().initAppLanguage(this);
    }

    @AfterInject
    void subscribePresenter() {
        presenter.subscribe();
    }

    @AfterViews
    protected void initUI() {
        initHomeFragment();
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        enableViews(getSupportFragmentManager().getBackStackEntryCount() > 1); //change here from true to expression
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        if (header != null) {
            ivAvatar_VDH = header.findViewById(R.id.ivAvatar_VDH);
            tvName_VDH = header.findViewById(R.id.tvTitle_VDH);
        }
        refreshDrawerHeader();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> enableViews(getSupportFragmentManager().getBackStackEntryCount() > 1));

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                                     @Override
                                                     public void onDrawerSlide(View drawer, float slideOffset) {
                                                         Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
                                                         ivAvatar_VDH.startAnimation(anim);
                                                     }

                                                     @Override
                                                     public void onDrawerClosed(View drawerView) {
                                                     }
                                                 }
        );
    }

    @AfterViews
    void initRxPermissions() {
        rxPermissions = new RxPermissions(this);
        rxPermissions.setLogging(BuildConfig.DEBUG);
    }

    public RxPermissions getRxPermissions() {
        return rxPermissions;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public String getScreenName() {
        return "Main screen";
    }

    @Override
    public void showPlacesCount(Integer cnt) {
        //placeCountView.setText(String.valueOf(cnt));
    }

    @Override
    protected int getContainer() {
        return R.id.flFragmentContent_AD;
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    protected void initHomeFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            replaceFirstFragment(HomeFragment_.builder().build());
            navigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    private void enableViews(boolean enable) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (enable) {
            // Remove hamburger
            drawerToggle.setDrawerIndicatorEnabled(false);
            // Show updateStatus button
            toolbarManager.showHomeAsUp(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            // Remove updateStatus button
            toolbarManager.showHomeAsUp(false);
            // Show hamburger
            drawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            mToolBarNavigationListenerIsRegistered = false;
        }

        drawerToggle.setToolbarNavigationClickListener(toolbarManager.getNavigationClickListener(mToolBarNavigationListenerIsRegistered));

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }

    public void refreshDrawerHeader() {
    }

    public void updateNavigationItem(@IdRes int id, boolean isChecked) {
        navigationView.getMenu().findItem(id).setChecked(isChecked);
    }

    public void enableNavigationItem(@IdRes int id, boolean enabled) {
        navigationView.getMenu().findItem(id).setEnabled(enabled);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();

        if (item.isChecked()) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.menuHome:
                replaceFragmentClearBackstack(HomeFragment_.builder().build());
                break;
            case R.id.menuPlaces:
                replaceFragmentClearBackstack(PlacesFragment_.builder().build());
                break;
            case R.id.menuSettings:
                replaceFragmentClearBackstack(SettingsFragment_.builder().build());
                break;
            case R.id.menuReceipts:
                //replaceFragmentClearBackstack(PlacesFragment_.builder().build());
                break;
        }
        return true;
    }

    @AfterViews
    void checkPermission() {
        if (rxPermissions == null) return;
        if (!rxPermissions.isGranted(PERMISSION_WRITE_EXTERNAL_STORAGE))
            requestPermission();
    }

    @SuppressLint("CheckResult")
    private void requestPermission() {
        rxPermissions
                .requestEach(PERMISSION_WRITE_EXTERNAL_STORAGE)
                .subscribe(permission -> {
                    if (permission.granted) {

                    } else if (permission.shouldShowRequestPermissionRationale) {
                        displayInfoAboutPermission();
                    } else {
                        displayInfoAboutGrantPermissionManually();
                    }
                });
    }

    @OnActivityResult(PERMISSIONS_SETTINGS_RESULT_CODE)
    void resultFromPermissionsSettings() {
        checkPermission();
    }

    private void displayInfoAboutPermission() {
        Snackbar snackbar = Snackbar.make(flFragmentContent_AD, R.string.msg_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.permission_grant, v -> requestPermission())
                .setDuration(4200);
        fixTextStyleInSnackbar(snackbar);
        snackbar.show();
    }

    private void displayInfoAboutGrantPermissionManually() {
        Snackbar snackbar = Snackbar.make(flFragmentContent_AD, R.string.msg_store_permission_not_granted, Snackbar.LENGTH_LONG)
                .setAction(R.string.permission_settings, v -> {
                    openApplicationSettings();
                    Toast.makeText(this,
                            R.string.msg_location_permission_on_tips,
                            Toast.LENGTH_SHORT)
                            .show();
                });
        fixTextStyleInSnackbar(snackbar);
        snackbar.show();
    }

    private void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSIONS_SETTINGS_RESULT_CODE);
    }

    private void fixTextStyleInSnackbar(final Snackbar snackbar) {
        TextView tv = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        TextView tvAction = snackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
        tv.setMaxLines(6);
        tv.setMinLines(2);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        tvAction.setTypeface(tvAction.getTypeface(), Typeface.BOLD);
    }
}
