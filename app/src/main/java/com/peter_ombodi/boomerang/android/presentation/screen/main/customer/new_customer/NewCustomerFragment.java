package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_customer)
public class NewCustomerFragment extends MVPFragment<NewCustomerContract.NewCustomerPresenter> implements NewCustomerContract.NewCustomerView {

    @ViewById(R.id.tvCustomerFirstName_FC)
    TextInputEditText firstNameView;
    @ViewById(R.id.tvCustomerLastName_FC)
    TextInputEditText lastNameView;
    @ViewById(R.id.tvCustomerPhone_FC)
    TextInputEditText phoneView;
    @ViewById(R.id.tvVehicleNumber_FC)
    TextInputEditText vehicleNumberView;
    @ViewById(R.id.tvEmail_FC)
    TextInputEditText emailView;

    @Bean
    protected NewCustomerInteractor interactor;

    @AfterInject
    @Override
    public void initPresenter() {
        new NewCustomerPresenter(this, interactor);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(R.string.title_new_customer);
    }

    @Click(R.id.btnSave_FC)
    void onSaveClick() {
        Customer customer = new Customer();
        customer.setFirstName(firstNameView.getText().toString());
        customer.setLastName(lastNameView.getText().toString());
        customer.setPhone(phoneView.getText().toString());
        customer.setEmail(emailView.getText().toString());
        customer.setVehicleNumber(vehicleNumberView.getText().toString());
        presenter.onSaveClick(customer);
    }

    @Click(R.id.btnCancel_FC)
    void onCancelClick() {
        activity.onBackPressed();
    }
    @Override
    public String getScreenName() {
        return "NewCustomerFragment";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_customer;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onCustomerSaved(Customer customer) {
        hideKeyboard();
        final Intent intentResult = new Intent();
        intentResult.putExtra(Constants.TAG_CUSTOMER, customer);
        getTargetFragment().onActivityResult(Constants.CUSTOMER_ADDED, Activity.RESULT_OK, intentResult);
        activity.onBackPressed();
    }
}
