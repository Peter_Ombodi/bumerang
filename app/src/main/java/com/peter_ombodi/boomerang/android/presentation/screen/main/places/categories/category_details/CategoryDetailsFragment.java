package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.ColorItem;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.CustomSpinner;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.ActionItem;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 03.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_category_details)
@OptionsMenu(R.menu.menu_delete_item)
public class CategoryDetailsFragment extends MVPFragment<CategoryDetailsContract.CategoryPresenter> implements CategoryDetailsContract.CategoryView {

    private AlertDialog alertDialog;

    @FragmentArg
    PlaceCategory placeCategory;

    @ViewById(R.id.tvCategoryName_FCD)
    TextInputEditText nameView;
    @ViewById(R.id.tvPrice_FCD)
    TextInputEditText priceView;
    @ViewById(R.id.spColor_FCD)
    CustomSpinner colorSpinner;
    @OptionsMenuItem(R.id.menuDeleteItem)
    protected MenuItem menuDeleteItem;

    @Bean
    protected CategoryDetailsInteractor interactor;

    @AfterInject
    @Override
    public void initPresenter() {
        new CategoryDetailsPresenter(this, interactor);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(R.string.edit_category_info);
        List<ActionItem> spinnerItems = new ArrayList<>();
        int i = 0;
        for (ColorItem colorItem : ColorsUtils.getColorItems()) {
            spinnerItems.add(new ActionItem(i++, colorItem.getLabel(), R.drawable.ic_box_unchecked, colorItem.getColor()));
        }
        colorSpinner.setIconRight(20);
        colorSpinner.setIconBottom(40);
        colorSpinner.setDropDownItems(spinnerItems);
        if (placeCategory != null) {
            nameView.setText(placeCategory.getName());
            priceView.setText(String.valueOf(placeCategory.getDefaultPrice()));
            colorSpinner.setItem(placeCategory.getColor());
        } else {
            colorSpinner.setItem(0);
        }
        colorSpinner.setInputType(InputType.TYPE_NULL);
        colorSpinner.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                hideKeyboard();
                colorSpinner.callOnClick();
            }
        });
    }

    @Click(R.id.btnSave_FCD)
    void onSaveClick() {
        if (placeCategory == null)
            placeCategory = new PlaceCategory();
        placeCategory.setName(nameView.getText().toString());
        placeCategory.setDefaultPrice(Integer.valueOf(priceView.getText().toString()));
        placeCategory.setColor(colorSpinner.getIdItem());
        presenter.onSaveClick(placeCategory);
    }

    @OptionsItem(R.id.menuDeleteItem)
    void onDeleteItemClick() {
        alertDialog = new AlertDialog.Builder(getContext(), R.style.DialogTheme)
                .setMessage(R.string.question_delete_category)
                .setNegativeButton(R.string.button_cancel, null)
                .setPositiveButton(R.string.button_yes, (dialog, which) -> {
                    presenter.onDeleteClick(placeCategory.getId());
                    dialog.dismiss();
                })
                .create();
        alertDialog.show();
        TextView messageView = alertDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }


    @Click(R.id.btnCancel_FCD)
    void onCancelClick() {
        activity.onBackPressed();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuDeleteItem.setVisible(placeCategory != null);
    }

    @Override
    public String getScreenName() {
        return "CategoryFragment";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_category_details;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onCategorySaved(PlaceCategory placeCategory) {
        hideKeyboard();
        final Intent intentResult = new Intent();
        intentResult.putExtra(Constants.TAG_CATEGORY, placeCategory);
        getTargetFragment().onActivityResult(Constants.CATEGORY_SAVED, Activity.RESULT_OK, intentResult);
        activity.onBackPressed();
    }

    @Override
    public void onCategoryDeleted() {
        activity.onBackPressed();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (alertDialog != null) alertDialog.dismiss();
    }
}
