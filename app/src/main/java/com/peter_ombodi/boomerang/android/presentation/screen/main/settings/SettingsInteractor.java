package com.peter_ombodi.boomerang.android.presentation.screen.main.settings;

import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.EBean;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class SettingsInteractor  extends BaseInteractor implements SettingsContract.SettingsModel {

}