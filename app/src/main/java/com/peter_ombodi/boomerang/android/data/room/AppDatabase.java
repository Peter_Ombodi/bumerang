package com.peter_ombodi.boomerang.android.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.peter_ombodi.boomerang.android.data.room.dao.CategoryAndPlacesDao;
import com.peter_ombodi.boomerang.android.data.room.dao.ICustomerDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IOrderDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IUserDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TCustomer;
import com.peter_ombodi.boomerang.android.data.room.tables.TOrder;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlace;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;
import com.peter_ombodi.boomerang.android.data.room.tables.TUser;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Database(entities = {TUser.class,TOrder.class,TPlace.class,TPlaceCategory.class,TCustomer.class}, version = 1)
public abstract class AppDatabase  extends RoomDatabase {
    public abstract IOrderDao orderDao();
    public abstract IPlaceDao placeDao();
    public abstract IPlaceCategoryDao placeCategoryDao();
    public abstract IUserDao userDao();
    public abstract ICustomerDao customerDao();
    public abstract CategoryAndPlacesDao categoryAndPlacesDao();
}
