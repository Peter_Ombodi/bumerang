package com.peter_ombodi.boomerang.android.presentation.screen.main.home;

import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.EBean;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class HomeInteractor  extends BaseInteractor implements HomeContract.HomeModel {


}
