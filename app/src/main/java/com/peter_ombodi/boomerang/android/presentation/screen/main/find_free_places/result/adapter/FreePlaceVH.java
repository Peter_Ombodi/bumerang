package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class FreePlaceVH extends RecyclerView.ViewHolder implements IProviderItemClickListener<PlaceFree> {

    private TextView sectorNameView;
    private TextView categoryNameView;
    private TextView priceView;
    private TextView freeFromView;
    private TextView freeUntilView;
    private View markerView;


    public FreePlaceVH(View itemView) {
        super(itemView);
        sectorNameView = itemView.findViewById(R.id.tvPlaceName_VISF);
        categoryNameView = itemView.findViewById(R.id.tvCategoryName_VISF);
        priceView = itemView.findViewById(R.id.tvPrice_VISF);
        freeFromView = itemView.findViewById(R.id.tvFrom_VISF);
        freeUntilView = itemView.findViewById(R.id.tvUntil_VISF);
        markerView = itemView.findViewById(R.id.vMarker_VISF);
    }

    public void bind(PlaceFree item) {
        itemView.setTag(item);
        sectorNameView.setText(item.getName());
        categoryNameView.setText(item.getCategoryName());
        markerView.setBackgroundColor(ColorsUtils.getColorItems().get(item.getColorId()).getColor());
        freeFromView.setText(DateManager.convertOrderDate(item.getFreeFrom()));
        freeUntilView.setText(DateManager.convertOrderDate(item.getFreeUntil()));
        priceView.setText(String.valueOf(item.getPrice()));
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<PlaceFree> listener) {
        itemView.setOnClickListener(view -> listener.onClick(view, (PlaceFree) view.getTag()));
    }
}
