package com.peter_ombodi.boomerang.android.data.room;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.peter_ombodi.boomerang.android.data.room.dao.CategoryAndPlacesDao;
import com.peter_ombodi.boomerang.android.data.room.dao.ICustomerDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IOrderDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IUserDao;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean(scope = EBean.Scope.Singleton)
public class RoomBase {

    private static final String DATABASE_NAME = "database-boomerang";

    @RootContext
    Context appContext;

    private AppDatabase appDatabase;

    @AfterInject
    void initDatabase() {
        appDatabase = Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME).build();
    }

    public IUserDao getUserDao() {
        return appDatabase.userDao();
    }

    public ICustomerDao getCustomerDao() {
        return appDatabase.customerDao();
    }

    public IOrderDao getOrderDao() {
        return appDatabase.orderDao();
    }

    public IPlaceCategoryDao getPlaceCategoryDao() {
        return appDatabase.placeCategoryDao();
    }

    public IPlaceDao getPlaceDao() {
        return appDatabase.placeDao();
    }

    public CategoryAndPlacesDao getCategoryAndPlacesDao() {
        return appDatabase.categoryAndPlacesDao();
    }

}

