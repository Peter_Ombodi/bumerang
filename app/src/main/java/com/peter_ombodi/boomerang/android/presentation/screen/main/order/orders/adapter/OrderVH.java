package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class OrderVH extends RecyclerView.ViewHolder implements IProviderItemClickListener<OrderFlat> {

    private TextView fromView;
    private TextView toView;
    private TextView placeNameView;
    private TextView customerView;
    private TextView phoneView;
    private TextView vehicleView;
    private TextView emailView;
    private TextView totalView;
    private View markerView;

    public OrderVH(View itemView) {
        super(itemView);
        fromView = itemView.findViewById(R.id.tvFrom_VIO);
        toView = itemView.findViewById(R.id.tvTo_VIO);
        placeNameView = itemView.findViewById(R.id.tvPlaceName_VIO);
        customerView = itemView.findViewById(R.id.tvCustomer_VIO);
        phoneView = itemView.findViewById(R.id.tvPhone_VIO);
        vehicleView = itemView.findViewById(R.id.tvVehicle_VIO);
        emailView = itemView.findViewById(R.id.tvEmail_VIO);
        totalView = itemView.findViewById(R.id.tvTotal_VIO);
        markerView = itemView.findViewById(R.id.vMarker_VIO);
    }

    public void bind(OrderFlat item) {
        itemView.setTag(item);
        fromView.setText(DateManager.convertOrderDate(item.getStart_date()));
        toView.setText(DateManager.convertOrderDate(item.getEnd_date()));
        placeNameView.setText(item.getPlaceName());
        if (item.getColor() != null)
            markerView.setBackgroundColor(ColorsUtils.getColorItems().get(item.getColor()).getColor());
        customerView.setText(String.format("%s %s", item.getFirstName(), item.getLastName()));
        phoneView.setText(item.getPhone());
        vehicleView.setText(item.getVehicleNumber());
        emailView.setText(item.getEmail());
        totalView.setText(String.valueOf(item.getTotal()));
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<OrderFlat> listener) {
        itemView.setOnClickListener(view -> listener.onClick(view, (OrderFlat) view.getTag()));
    }
}
