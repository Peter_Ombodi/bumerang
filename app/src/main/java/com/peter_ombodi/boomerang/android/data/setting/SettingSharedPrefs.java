package com.peter_ombodi.boomerang.android.data.setting;

import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface SettingSharedPrefs {

    @DefaultString("Boomerang")
    String billTitle();

    @DefaultString("lake of your dreams")
    String billSubtitle();

    @DefaultString("1, Dejda pit, Beregszasz district")
    String address();

    @DefaultString("+38 (067) 336-51-61")
    String phoneNumber();

    @DefaultString("You will never swim alone")
    String slogan();

    @DefaultString("mail@boomerang.com")
    String email();

    @DefaultString(Constants.LOCALE_LANG_UK)
    String appLocaleLanguage();

    @DefaultString(Constants.LOCALE_REGION_UK)
    String appLocaleRegion();
}
