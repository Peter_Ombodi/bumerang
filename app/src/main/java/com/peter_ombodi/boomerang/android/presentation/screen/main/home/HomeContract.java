package com.peter_ombodi.boomerang.android.presentation.screen.main.home;

import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface HomeContract {

    interface HomeView extends IBaseContentView<HomePresenter> {
    }
    interface HomePresenter extends IBasePresenter {
    }
    interface HomeModel extends IBaseModel {
    }
}
