package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IOrderDao;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

import static com.peter_ombodi.boomerang.android.presentation.utils.DateManager.convertPickerDate;

/**
 * @author Peter Ombodi (Created on 10.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class FindResultInteractor extends BaseInteractor implements FindResultContract.FindResultModelI {

    @Bean
    RoomBase roomBase;

    private IOrderDao orderDao;

    @AfterInject
    void init() {
        orderDao = roomBase.getOrderDao();
    }


    @Override
    public Observable<List<PlaceFree>> getFreePlaces(Date startDate, Date endDate, Integer minPrice, Integer maxPrice) {
        return getAsyncObservable(orderDao.getFreePlaces(convertPickerDate(startDate), convertPickerDate(endDate), minPrice, maxPrice).toObservable());
    }
}
