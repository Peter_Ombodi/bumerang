package com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details;

import android.support.annotation.IntDef;

import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface OrderDetailsContract {

    @IntDef({ORDER_NEW, ORDER_PAID, ORDER_BOOKED, ORDER_CANCELED})
    @Retention(RetentionPolicy.RUNTIME)
    @interface State {
    }

    int ORDER_NEW = 0;
    int ORDER_PAID = 1;
    int ORDER_BOOKED = 2;
    int ORDER_CANCELED = 3;

    interface OrderDetailsView extends IBaseContentView<OrderDetailsPresenter> {
        void showHomeScreen();
        void setCategory(String category);
        void setupOrderInfo();
        void showBill(OrderHeader orderHeader);
        void onOrderInfoUpdated(OrderHeader orderHeader);
    }

    interface OrderDetailsPresenter extends IBasePresenter {
        void updateOrderInfo(OrderHeader orderHeader);
        void onSaveOrderClick(OrderHeader orderHeader);
        void onCancelClick();
        void setViewData(OrderHeader orderHeader);
        void setCustomer(Customer customer);
    }

    interface OrderDetailsModel extends IBaseModel {
        Observable<Boolean> saveOrder(OrderHeader orderHeader);
        Observable<OrderHeader> updateOrderInfo(OrderHeader orderHeader);
    }
}
