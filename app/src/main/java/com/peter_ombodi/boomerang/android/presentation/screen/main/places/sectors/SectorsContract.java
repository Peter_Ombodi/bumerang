package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public interface SectorsContract {

    interface SectorsView extends IBaseContentView<SectorsPresenter> {
        void showSectors(List<PlaceFlat> list);
        void editSector(PlaceItem item);
    }

    interface SectorsPresenter extends IBasePresenter {
        void getSectors();
        void onItemSelected(PlaceFlat place);
        Observable<List<PlaceFlat>> filterSectors(String filter);
    }

    interface SectorsModel extends IBaseModel {
        Observable<List<PlaceFlat>> getSectors();
    }
}

