package com.peter_ombodi.boomerang.android.presentation.base.tabs;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.custom_view.ScrollableViewPager;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment
public abstract class MVPTabsFragment extends MVPFragment {

    public TabPagerAdapter tabPagerAdapter;

    @ViewById(R.id.tlTabs_FCT)
    protected TabLayout tlTabs_FCT;
    @ViewById(R.id.vpContent_FCT)
    public ScrollableViewPager vpContent_FCT;

    protected FragmentManager childFragmentManager;

    @AfterInject
    protected void initData() {
        childFragmentManager = getChildFragmentManager();
        tabPagerAdapter = new TabPagerAdapter(getChildFragmentManager());
        addFragmentsToAdapter(tabPagerAdapter);
    }

    public abstract void addFragmentsToAdapter(TabPagerAdapter adapter);

    @AfterViews
    protected void initUI() {
        activity.getToolbarManager().enableToolbarElevation(false);
        vpContent_FCT.setAdapter(tabPagerAdapter);
        tlTabs_FCT.setupWithViewPager(vpContent_FCT);

        vpContent_FCT.addOnPageChangeListener(onPageChangeListener);
    }

    protected void showTabs(boolean visible) {
        tlTabs_FCT.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activity.getToolbarManager().enableToolbarElevation(true);
    }

    public ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };
}
