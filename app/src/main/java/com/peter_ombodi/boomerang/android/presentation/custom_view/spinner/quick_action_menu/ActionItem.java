package com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu;

/**
 * @author Peter Ombodi (Created on 28.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class ActionItem {
    private int icon;
    private int iconColor;
    private String title;
    private int actionId = -1;
    private boolean enabled = true;

    public ActionItem(int _actionId, String _title, int _icon) {
        this.title = _title;
        this.icon = _icon;
        this.actionId = _actionId;
        this.iconColor = 0;
    }

    public ActionItem(int _actionId, String _title, int _icon, int _iconColor) {
        this.title = _title;
        this.icon = _icon;
        this.iconColor = _iconColor;
        this.actionId = _actionId;
    }

    public ActionItem(int _actionId, String _title, int _icon, int _iconColor, boolean _enabled) {
        this.title = _title;
        this.icon = _icon;
        this.iconColor = _iconColor;
        this.actionId = _actionId;
        this.enabled = _enabled;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int _icon) {
        this.icon = _icon;
    }

    public void setTitle(String _title) {
        this.title = _title;
    }

    public String getTitle() {
        return this.title;
    }

    public int getActionId() {
        return actionId;
    }

    public int getIconColor() {
        return iconColor;
    }

    public void setIconColor(int iconColor) {
        this.iconColor = iconColor;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "ActionItem{" +
                "icon=" + icon +
                ", iconColor=" + iconColor +
                ", title='" + title + '\'' +
                ", actionId=" + actionId +
                '}';
    }
}
