package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders;

import android.text.TextUtils;

import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Observable;

import static com.peter_ombodi.boomerang.android.presentation.utils.DateManager.convertPickerDate;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class OrdersPresenter extends BasePresenter implements OrdersContract.OrdersPresenter {

    private OrdersContract.OrdersView view;
    private OrdersContract.OrdersModel model;
    private List<OrderFlat> orderFlatList;

    public OrdersPresenter(OrdersContract.OrdersView view, OrdersContract.OrdersModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void getSoldPlaces(int orderFilter) {
        view.showProgressMain();
        compositeSubscriptions.add(
                model.getSoldPlaces(orderFilter,convertPickerDate(Calendar.getInstance().getTime()))
                        .subscribe(orderFlats -> {
                                    view.showResult(orderFlats);
                                    orderFlatList = orderFlats;
                                    view.hideProgress();
                                },
                                throwable -> {
                                    view.hideProgress();
                                    throwable.printStackTrace();
                                }));

    }

    @Override
    public void getTodayArrivePlaces() {

    }

    @Override
    public void getTodayLeavePlaces() {

    }

    @Override
    public void onPlaceSelected(OrderFlat orderHeader) {

    }

    @Override
    public Observable<List<OrderFlat>> filterOrders(String filter) {
        return Observable.fromCallable(() -> {
            if (!TextUtils.isEmpty(filter)) {
                return filterOrders(orderFlatList, filter);
            } else return orderFlatList;
        });
    }

    private List<OrderFlat> filterOrders(List<OrderFlat> fullList, String query) {
        final List<OrderFlat> list = new ArrayList<>();
        for (OrderFlat item : fullList) {
            if (containsQuery(item.getFirstName(), query) ||
                    containsQuery(item.getLastName(), query) ||
                    containsQuery(item.getPhone(), query) ||
                    containsQuery(item.getEmail(), query) ||
                    containsQuery(item.getPlaceName(), query) ||
                    containsQuery(item.getVehicleNumber(), query)) {
                list.add(item);
            }
        }
        return list;
    }

    private boolean containsQuery(String field, String query) {
        return (!TextUtils.isEmpty(field) && field.toLowerCase().contains(query.toLowerCase()));
    }
}
