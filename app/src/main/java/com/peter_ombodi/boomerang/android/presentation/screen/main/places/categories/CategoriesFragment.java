package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories;

import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.adapter.CategoriesAdapter;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details.CategoryDetailsFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details.CategoryDetailsFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_categories_list)
public class CategoriesFragment extends MVPFragment<CategoriesContract.CategoriesPresenter> implements CategoriesContract.CategoriesView {


    @Bean
    protected CategoriesInteractor interactor;

    @Bean
    CategoriesAdapter adapter;

    @ViewById(R.id.rvCategories_FCL)
    RecyclerView recyclerView;

    @ViewById(R.id.etSearch_FSO)
    EditText filterView;

    @AfterInject
    @Override
    public void initPresenter() {
        new CategoriesPresenter(this, interactor);
    }

    @AfterViews
    protected void initFindView() {
        if (presenter == null)
            initPresenter();
        presenter.getCategories();

    }

    @Override
    public String getScreenName() {
        return "Categories Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_categories_list;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public void showCategories(List<PlaceCategory> list) {
        adapter.setData(list);
        adapter.setOnItemClickListener((view, item) -> presenter.onItemSelected(item));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void editCategory(PlaceCategory placeCategory) {
        CategoryDetailsFragment fragment = CategoryDetailsFragment_.builder()
                .placeCategory(placeCategory)
                .build();
        fragment.setTargetFragment(getParentFragment(), Constants.CATEGORY_SAVED);
        activity.replaceFragment(fragment);
    }
}
