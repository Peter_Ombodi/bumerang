package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

@Entity(tableName = "place_categories")
public class TPlaceCategory {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private String name;

    @ColumnInfo(name = "default_price")
    private Integer defaultPrice;

    private Integer color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Integer defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public PlaceCategory toPlaceCategory(){
        PlaceCategory convertedCategory =  new PlaceCategory();
        convertedCategory.setId(getId());
        convertedCategory.setColor(getColor());
        convertedCategory.setDefaultPrice(getDefaultPrice());
        convertedCategory.setName(getName());
        return convertedCategory;
    }

    public static TPlaceCategory fromPlaceCategory(PlaceCategory placeCategory){
        TPlaceCategory convertedCategory =  new TPlaceCategory();
        convertedCategory.setId(placeCategory.getId());
        convertedCategory.setColor(placeCategory.getColor());
        convertedCategory.setDefaultPrice(placeCategory.getDefaultPrice());
        convertedCategory.setName(placeCategory.getName());
        return convertedCategory;
    }

    @Override
    public String toString() {
        return "TPlaceCategory{" +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", defaultPrice=" + defaultPrice +
                ", color=" + color +
                '}';
    }
}
