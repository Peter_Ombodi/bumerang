package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "users")
public class TUser {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "is_active")
    private int isActive;

    @ColumnInfo(name = "is_admin")
    private int isAdmin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

//    public static TUser fromUser(User user) {
//        final TUser convertedUser = new TUser();
//        convertedUser.setFirstName(user.getFirstName());
//        convertedUser.setLastName(user.getLastName());
//        convertedUser.setEmail(user.getEmail());
//        convertedUser.setId(user.getId());
//        convertedUser.setIsActive(user.isActive() ? 1 : 0);
//        convertedUser.setIsAdmin(user.isAdmin() ? 1 : 0);
//        return convertedUser;
//    }
//
//    public User toUser() {
//        final User convertedUser = new User();
//        convertedUser.setFirstName(getFirstName());
//        convertedUser.setLastName(getLastName());
//        convertedUser.setEmail(getEmail());
//        convertedUser.setId(getId());
//        convertedUser.setActive(getIsActive() == 1);
//        convertedUser.setAdmin(getIsAdmin() == 1);
//        return convertedUser;
//    }
}
