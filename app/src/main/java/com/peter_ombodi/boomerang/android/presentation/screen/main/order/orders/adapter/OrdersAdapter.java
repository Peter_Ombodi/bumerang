package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class OrdersAdapter extends RecyclerView.Adapter<OrderVH> implements IProviderItemClickListener<OrderFlat> {

    private IItemClickListener<OrderFlat> clickListener;
    private List<OrderFlat> items;

    public void setData(List<OrderFlat> list) {
        items = list;
        notifyDataSetChanged();
    }

    public void clear() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }

    public void notifySlot(PlaceItem item) {
        notifyItemChanged(items.indexOf(item));
    }

    @NonNull
    @Override
    public OrderVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OrderVH vh = new OrderVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_order, parent, false));
        vh.setOnItemClickListener(clickListener);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull OrderVH holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<OrderFlat> listener) {
        this.clickListener = listener;
    }
}
