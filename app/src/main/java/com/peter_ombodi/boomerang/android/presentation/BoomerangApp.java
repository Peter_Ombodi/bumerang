package com.peter_ombodi.boomerang.android.presentation;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.peter_ombodi.boomerang.android.BuildConfig;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.setting.SettingsManager;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;
import com.peter_ombodi.boomerang.android.presentation.utils.LocaleUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@SuppressLint("Registered")
@EApplication
public class BoomerangApp extends Application {

    /**
     * Just start initialize
     **/
//    @Bean
//    Rest rest;

    @Bean
    RoomBase roomBase;

    @Bean
    SettingsManager settingsManager;

    @AfterInject
    void initStetho() {
        BuildConfig.STETHO.init(this);
    }

    @AfterInject
    void initEnvironment() {
        ColorsUtils.initEventColors(getApplicationContext());
    }

    public void initAppLanguage(Context context) {
        LocaleUtils.setAppLocale(context, settingsManager.getLocaleLanguage());
    }
}
