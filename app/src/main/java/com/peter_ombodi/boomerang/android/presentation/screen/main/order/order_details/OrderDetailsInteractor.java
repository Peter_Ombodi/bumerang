package com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details;

import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.ICustomerDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IOrderDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TCustomer;
import com.peter_ombodi.boomerang.android.data.room.tables.TOrder;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class OrderDetailsInteractor extends BaseInteractor implements OrderDetailsContract.OrderDetailsModel {

    @Bean
    RoomBase roomBase;

    private IOrderDao orderDao;
    private ICustomerDao customerDao;

    @AfterInject
    void init() {
        orderDao = roomBase.getOrderDao();
        customerDao = roomBase.getCustomerDao();
    }

    @Override
    public Observable<Boolean> saveOrder(OrderHeader orderHeader) {
        return getCustomerById(orderHeader.getCustomer().getId())
                .flatMap(customer -> {
                    if (customer.getHash() == orderHeader.getCustomer().getHash())
                        return Observable.fromCallable(() -> orderHeader.getCustomer().getId());
                    else {
                        return saveCustomer(orderHeader.getCustomer());
                    }
                })
                .flatMap(customerId -> {
                    orderHeader.getCustomer().setId(customerId);
                    TOrder tOrder = TOrder.fromOrder(orderHeader);
                    return getAsyncObservable(Observable.fromCallable(() -> {
                        orderDao.insertOrder(tOrder);
                        return true;
                    }));
                });
    }

    @Override
    public Observable<OrderHeader> updateOrderInfo(OrderHeader orderHeader) {
        return getAsyncObservable(orderDao
                .getOrderInfo(orderHeader.getStartDateConverted(),
                        orderHeader.getEndDateConverted(),
                        String.valueOf(orderHeader.getPlaceItem().getId()))
                .map(tOrder -> {
                    if (tOrder != null) {
                        orderHeader.setStatus(tOrder.getStatus());
                        orderHeader.setBillDate(tOrder.getBillDate());
                    }
                    return orderHeader;
                }).toObservable());
    }

    private Observable<Customer> getCustomerById(long id) {
        return getAsyncObservable(customerDao.getCustomerById(id).toObservable().map(TCustomer::fromTCustomer));
    }

    private Observable<Long> saveCustomer(Customer customer) {
        getCustomerById(customer.getId());
        TCustomer tCustomer = TCustomer.fromCustomer(customer);
        return getAsyncObservable(Observable.fromCallable(() -> customerDao.insertCustomer(tCustomer)));
    }
}
