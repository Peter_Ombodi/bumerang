package com.peter_ombodi.boomerang.android.develop;

import android.app.Application;

import okhttp3.OkHttpClient;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class ReleaseStethoHelper implements IStethoConfigure {
    @Override
    public void init(Application application) {
        // Nothing
    }

    @Override
    public void configureInterceptor(OkHttpClient.Builder httpClient) {
        // Nothing
    }
}
