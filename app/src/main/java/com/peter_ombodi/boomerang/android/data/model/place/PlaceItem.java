package com.peter_ombodi.boomerang.android.data.model.place;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class PlaceItem implements Serializable {

    private Long id;

    @SerializedName("place_category")
    private PlaceCategory placeCategory;

    private String name;

    private Integer price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PlaceCategory getPlaceCategory() {
        return placeCategory;
    }

    public void setPlaceCategory(PlaceCategory placeCategory) {
        this.placeCategory = placeCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PlaceItem{" +
                "id=" + id +
                ", placeCategory=" + placeCategory.toString() +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
