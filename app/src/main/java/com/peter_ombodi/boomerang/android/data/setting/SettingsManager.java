package com.peter_ombodi.boomerang.android.data.setting;

import com.peter_ombodi.boomerang.android.data.model.settings.BillSettings;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EBean(scope = EBean.Scope.Singleton)
public class SettingsManager {

    @Pref
    SettingSharedPrefs_ sharedPrefs;

    public void saveBillSettings(final BillSettings settings) {
        sharedPrefs.edit().billTitle().put(settings.getBillTitle()).apply();
        sharedPrefs.edit().billSubtitle().put(settings.getBillSubtitle()).apply();
        sharedPrefs.edit().address().put(settings.getAddress()).apply();
        sharedPrefs.edit().phoneNumber().put(settings.getPhone()).apply();
        sharedPrefs.edit().slogan().put(settings.getSlogan()).apply();
        sharedPrefs.edit().email().put(settings.getEmail()).apply();
    }

    public BillSettings getBillSettings() {
        BillSettings billSettings = new BillSettings();
        billSettings.setBillTitle(sharedPrefs.billTitle().get());
        billSettings.setBillSubtitle(sharedPrefs.billSubtitle().get());
        billSettings.setAddress(sharedPrefs.address().get());
        billSettings.setPhone(sharedPrefs.phoneNumber().get());
        billSettings.setSlogan(sharedPrefs.slogan().get());
        billSettings.setEmail(sharedPrefs.email().get());
        return billSettings;
    }

    public void saveLocaleLanguage(final String settings) {
        sharedPrefs.edit().appLocaleLanguage().put(settings).apply();
    }

    public String getLocaleLanguage(){
        return sharedPrefs.appLocaleLanguage().get();
    }

    public void saveLocaleRegion(final String settings) {
        sharedPrefs.edit().appLocaleRegion().put(settings).apply();
    }

    public String getLocaleRegion(){
        return sharedPrefs.appLocaleRegion().get();
    }

}