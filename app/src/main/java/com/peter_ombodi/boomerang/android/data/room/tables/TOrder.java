package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.room.converters.BillDateConverter;
import com.peter_ombodi.boomerang.android.data.room.converters.ShortDateConverter;

import java.util.Date;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "orders")
public class TOrder {
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "place_id")
    private Long placeId;

    @ColumnInfo(name = "start_date")
    @TypeConverters({ShortDateConverter.class})
    public Date startDate;

    @ColumnInfo(name = "end_date")
    @TypeConverters({ShortDateConverter.class})
    public Date endDate;

    @ColumnInfo(name = "bill_date")
    @TypeConverters({BillDateConverter.class})
    public Date billDate;

    private Integer status;

    @ColumnInfo(name = "user_id")
    private Integer userId;

    @ColumnInfo(name = "customer_id")
    private long customerId;

    private Integer total;

    private String vehicle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public static TOrder fromOrder(OrderHeader orderHeader) {
        final TOrder converted = new TOrder();
        converted.placeId= orderHeader.getPlaceItem().getId();
        converted.startDate= orderHeader.getStartDate();
        converted.endDate= orderHeader.getEndDate();
        converted.billDate= orderHeader.getBillDate();
        converted.total= orderHeader.getTotal();
        converted.status= orderHeader.getStatus();
        converted.vehicle= orderHeader.getVehicle();
        converted.customerId= orderHeader.getCustomer().getId();
        return converted;
    }
}
