package com.peter_ombodi.boomerang.android.presentation.screen.main;

import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class MainPresenter extends BasePresenter implements MainContract.MainPresenter {

    private MainContract.MainView view;
    private MainContract.MainModel model;


    public MainPresenter(MainContract.MainView view, MainContract.MainModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void checkPlacesCount() {
        compositeSubscriptions.add(
                model.getPlacesCount().subscribe(result -> {
                    view.showPlacesCount(result);
                    if (result == 0) {
                        model.fillPlaces().subscribe();
                        model.getPlacesCount().subscribe(result1 ->
                                view.showPlacesCount(result1));
                    } else {
                        //model.nukeTables().subscribe();
                    }
                }));
    }

    @Override
    public void subscribe() {

       checkPlacesCount();
    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }
}
