package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "order_vehicles", primaryKeys = {"order_id", "license_number"})
public class TOrderVehicle {
    @ColumnInfo(name = "order_id")
    private String orderId;

    @ColumnInfo(name = "license_number")
    private String licenseNumber;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }
}
