package com.peter_ombodi.boomerang.android.presentation.screen.main.order.bill;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.setting.SettingsManager;
import com.peter_ombodi.boomerang.android.presentation.custom_view.BillView;
import com.peter_ombodi.boomerang.android.presentation.utils.BitmapCompress;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.Objects;

import static android.support.v4.content.FileProvider.getUriForFile;

/**
 * @author Peter Ombodi (Created on 25.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_bill_share)
public class ShareBillFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<File> {

    static final int LOADER_BITMAP_ID = 1;

    @Bean
    SettingsManager settingsManager;

    @FragmentArg
    OrderHeader orderHeader;

    @ViewById(R.id.bill_FBS)
    BillView billView;
    @ViewById(R.id.linearLayout_FBS)
    LinearLayout rootLayoutView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }

    @AfterViews
    protected void initViews() {
        billView.setData(orderHeader, settingsManager.getBillSettings());
    }

    @Click(R.id.btnShare_FBS)
    void onShareClick() {
        Bitmap bitmap = billView.saveSignature();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BitmapCompress.ARGS_BITMAP, bitmap);
        File file = new File(Environment.getExternalStorageDirectory(),Constants.PATH_BILLS);
        if (!file.exists() && !file.mkdirs()) {
            dismiss();
            if (getTargetFragment() != null) {
                getTargetFragment().onActivityResult(Constants.BILL_SHARED, Constants.RESULT_SAVE_ERROR, null);
            }
        }
        String fileName = Constants.PATH_BILLS + orderHeader.getPlaceItem().getName() + "_" + DateManager.convertShortDate(orderHeader.getStartDate()) + ".jpg";
        bundle.putString(BitmapCompress.FILE_NAME, fileName);
        if (getLoaderManager().getLoader(LOADER_BITMAP_ID) == null) {
            getLoaderManager().initLoader(LOADER_BITMAP_ID, bundle, this);
        } else {
            getLoaderManager().restartLoader(LOADER_BITMAP_ID, bundle, this);
        }
    }

    @NonNull
    @Override
    public Loader<File> onCreateLoader(int id, Bundle args) {
        return new BitmapCompress(getActivity(), args);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<File> loader, File _data) {
        shareImage(_data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<File> loader) {

    }

    private void shareImage(File file) {
        Uri contentUri = getUriForFile(Objects.requireNonNull(getContext()), "com.peter_ombodi.boomerang.fileprovider", file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, contentUri);
        intent.putExtra("address", orderHeader.getCustomer().getPhone());
        intent.setDataAndType(contentUri, "image/*");
        try {
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_title)));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), getResources().getString(R.string.msg_no_app_share), Toast.LENGTH_SHORT).show();
        }
        dismiss();
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(Constants.BILL_SHARED, Activity.RESULT_OK, null);
        }
    }

//    private void shareImage1(File file) {
//        String viberContact = orderHeader.getCustomer().getPhone();
//        Uri uriFile = Uri.fromFile(file);
//        Uri uri = Uri.parse("smsto:" + viberContact);
//        Intent intent = new Intent("android.intent.action.VIEW");
//        intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
//        intent.setData(uri);
//        intent.setType("image/*");
//        intent.setPackage("com.viber.voip");
//        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
//        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
//        intent.putExtra(Intent.EXTRA_STREAM, uriFile);
//        intent.putExtra("address", viberContact);
//        startActivity(intent);
//        dismiss();
//    }
//
//
//    void intentMessageTelegram(File file) {
//        final String appName = "org.telegram.messenger";
//        final boolean isAppInstalled = isAppAvailable(getActivity(), appName);
//        Uri uriFile = Uri.fromFile(file);
//        if (isAppInstalled) {
//            String contact = orderHeader.getCustomer().getPhone();
//            Intent intent = new Intent(Intent.ACTION_SEND);
//            intent.setType("image/*");
//            intent.setPackage(appName);
//            intent.putExtra(Intent.EXTRA_STREAM, uriFile);
//            startActivity(Intent.createChooser(intent, "Share with"));
//        } else {
//            Toast.makeText(getActivity(), "Telegram not Installed", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public static boolean isAppAvailable(Context context, String appName) {
//        PackageManager pm = context.getPackageManager();
//        try {
//            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
//            return true;
//        } catch (PackageManager.NameNotFoundException e) {
//            return false;
//        }
//    }
}
