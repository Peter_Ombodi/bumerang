package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface FindCustomerContract {
    interface FindCustomerView extends IBaseContentView<FindCustomerContract.FindCustomerPresenter> {
        void showResult(List<Customer> items);
        void selectCustomer(Customer customer);
    }

    interface FindCustomerPresenter extends IBasePresenter {
        void getAllCustomers();
        Observable<List<Customer>> filterCustomers(String filterString);
        void onCustomerSelected(Customer item);
    }

    interface FindCustomerModel extends IBaseModel {
        Observable<List<Customer>> getAllCustomers();
    }
}
