package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details;

import android.util.Log;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 03.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public final class CategoryDetailsPresenter extends BasePresenter implements CategoryDetailsContract.CategoryPresenter {

    private CategoryDetailsContract.CategoryView view;
    private CategoryDetailsContract.CategoryModel model;

    public CategoryDetailsPresenter(CategoryDetailsContract.CategoryView view, CategoryDetailsContract.CategoryModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void onSaveClick(PlaceCategory placeCategory) {
        compositeSubscriptions.add(
                model.saveCategory(placeCategory)
                        .subscribe(newId -> {
                            Log.d("CategoryPresenter", "onSaveClick: Ok");
                            placeCategory.setId(newId.intValue());
                            view.onCategorySaved(placeCategory);
                        }, Throwable::printStackTrace));
    }

    @Override
    public void onDeleteClick(int categoryId) {
        compositeSubscriptions.add(
                model.deleteCategory(categoryId)
                        .subscribe(cnt -> {
                            if (cnt>0)
                                view.onCategoryDeleted();
                        }, Throwable::printStackTrace));
    }
}
