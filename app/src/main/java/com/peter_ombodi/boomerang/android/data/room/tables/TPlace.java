package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "places")
public class TPlace {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "category_id")
    private Integer categoryId;

    private String name;

    private Integer price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getStringCategoryId() {
        return String.valueOf(categoryId);
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public static TPlace fromPlace(PlaceItem item){
        TPlace convertedItem =  new TPlace();
        convertedItem.setId(item.getId());
        convertedItem.setName(item.getName());
        convertedItem.setPrice(item.getPrice());
        convertedItem.setCategoryId(item.getPlaceCategory().getId());
        return convertedItem;
    }

    public PlaceItem toPlace() {
        final PlaceItem convertedPlaceItem = new PlaceItem();
        convertedPlaceItem.setId(getId());
        convertedPlaceItem.setName(getName());
        convertedPlaceItem.setPrice(getPrice());
        return convertedPlaceItem;
    }
}
