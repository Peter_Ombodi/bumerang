package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find;

import android.text.TextUtils;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class FindCustomerPresenter extends BasePresenter implements FindCustomerContract.FindCustomerPresenter {

    private FindCustomerContract.FindCustomerView view;
    private FindCustomerContract.FindCustomerModel model;
    private List<Customer> fullCustomersList;

    public FindCustomerPresenter(FindCustomerContract.FindCustomerView view, FindCustomerContract.FindCustomerModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void getAllCustomers() {
        compositeSubscriptions.add(model.getAllCustomers().subscribe(customers -> {
            view.showResult(customers);
            fullCustomersList = customers;
        }, Throwable::printStackTrace));
    }

    @Override
    public Observable<List<Customer>> filterCustomers(String query) {
        return Observable.fromCallable(() -> {
            if (!TextUtils.isEmpty(query)) {
                return filterCustomers(fullCustomersList, query);
            } else return fullCustomersList;
        });
    }

    @Override
    public void onCustomerSelected(Customer customer) {
        view.selectCustomer(customer);
    }

    private List<Customer> filterCustomers(List<Customer> fullCustomersList, String query) {
        final List<Customer> customerList = new ArrayList<>();
        for (Customer customer : fullCustomersList) {
            if (containsQuery(customer.getFirstName(), query) ||
                    containsQuery(customer.getLastName(), query) ||
                    containsQuery(customer.getPhone(), query) ||
                    containsQuery(customer.getEmail(), query) ||
                    containsQuery(customer.getVehicleNumber(), query)) {
                customerList.add(customer);
            }
        }
        return customerList;
    }

    private boolean containsQuery(String field, String query) {
        return (!TextUtils.isEmpty(field) && field.toLowerCase().contains(query.toLowerCase()));
    }
}
