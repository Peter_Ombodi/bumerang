package com.peter_ombodi.boomerang.android.data.model.organization;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 25.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class OrganizationRequisites implements Serializable{
    private String title;
    @SerializedName("sub_title")
    private String subTitle;
    private String phone;
    private String email;
    private String address;
    private String slogan;

    public OrganizationRequisites(String title, String subTitle, String phone, String email, String address, String slogan) {
        this.title = title;
        this.subTitle = subTitle;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.slogan = slogan;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Override
    public String toString() {
        return "OrganizationRequisites{" +
                "title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", slogan='" + slogan + '\'' +
                '}';
    }
}
