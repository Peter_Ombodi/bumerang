package com.peter_ombodi.boomerang.android.data.model.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class AppUser extends User implements Serializable {

    @SerializedName("is_admin")
    private boolean isAdmin;

}
