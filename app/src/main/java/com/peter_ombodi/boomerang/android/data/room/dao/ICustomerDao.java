package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peter_ombodi.boomerang.android.data.room.tables.TCustomer;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Dao
public interface ICustomerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TCustomer... customers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCustomer(TCustomer customer);

    @Query("DELETE FROM customers")
    void nukeTable();

    @Query("SELECT * FROM customers WHERE id = :id LIMIT 1")
    Maybe<TCustomer> getCustomerById(Long id);

    @Query("SELECT * FROM customers")
    Maybe<List<TCustomer>> getAllCustomers();

    @Query("SELECT * FROM customers WHERE id IN (:ids)")
    Single<List<TCustomer>> getAllByIds(String[] ids);
}
