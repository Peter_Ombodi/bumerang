package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.CustomSpinner;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.ActionItem;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 09.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_sector_details)
@OptionsMenu(R.menu.menu_delete_item)
public class SectorDetailsFragment extends MVPFragment<SectorDetailsContract.SectorPresenter> implements SectorDetailsContract.SectorView {

    private AlertDialog alertDialog;

    @FragmentArg
    PlaceItem placeItem;

    @ViewById(R.id.tvName_FSD)
    TextInputEditText nameView;
    @ViewById(R.id.tvPrice_FSD)
    TextInputEditText priceView;
    @ViewById(R.id.spCategory_FSD)
    CustomSpinner categorySpinner;

    @OptionsMenuItem(R.id.menuDeleteItem)
    protected MenuItem menuDeleteItem;

    @Bean
    protected SectorDetailsInteractor interactor;

    @AfterInject
    @Override
    public void initPresenter() {
        new SectorDetailsPresenter(this, interactor);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(R.string.edit_sector_info);
        if (placeItem != null) {
            nameView.setText(placeItem.getName());
            priceView.setText(String.valueOf(placeItem.getPrice()));

        } else {

        }
        presenter.getCategories();
        categorySpinner.setInputType(InputType.TYPE_NULL);
        categorySpinner.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                hideKeyboard();
                categorySpinner.callOnClick();
            }
        });

    }

    @Click(R.id.btnSave_FSD)
    void onSaveClick() {
        if (placeItem == null)
            placeItem = new PlaceItem();
        placeItem.setName(nameView.getText().toString());
        placeItem.setPrice(Integer.valueOf(priceView.getText().toString()));
        PlaceCategory category = new PlaceCategory();
        category.setId(categorySpinner.getIdItem());
        placeItem.setPlaceCategory(category);
        presenter.onSaveClick(placeItem);
    }

    @OptionsItem(R.id.menuDeleteItem)
    void onDeleteItemClick() {
        alertDialog = new AlertDialog.Builder(getContext(), R.style.DialogTheme)
                .setMessage(R.string.question_delete_sector)
                .setNegativeButton(R.string.button_cancel, null)
                .setPositiveButton(R.string.button_yes, (dialog, which) -> {
                    presenter.onDeleteClick(placeItem.getId());
                    dialog.dismiss();
                })
                .create();
        alertDialog.show();
        TextView messageView = alertDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }


    @Click(R.id.btnCancel_FSD)
    void onCancelClick() {
        activity.onBackPressed();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuDeleteItem.setVisible(placeItem != null);
    }

    @Override
    public String getScreenName() {
        return "SectorFragment";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_sector_details;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onSectorSaved(PlaceItem item) {
        hideKeyboard();
        final Intent intentResult = new Intent();
        intentResult.putExtra(Constants.TAG_SECTOR, item);
        getTargetFragment().onActivityResult(Constants.SECTOR_SAVED, Activity.RESULT_OK, intentResult);
        activity.onBackPressed();
    }

    @Override
    public void onSectorDeleted() {
        activity.onBackPressed();
    }

    @Override
    public void setCategories(List<PlaceCategory> list) {
        List<ActionItem> spinnerItems = new ArrayList<>();
        for (PlaceCategory item : list) {
            spinnerItems.add(new ActionItem(item.getId(), item.getName(), R.drawable.ic_box_unchecked, ColorsUtils.getColorItems().get(item.getColor()).getColor()));
        }
        categorySpinner.setIconRight(20);
        categorySpinner.setIconBottom(40);
        categorySpinner.setDropDownItems(spinnerItems);
        if (placeItem == null)
            categorySpinner.setItem(0);
        else
            categorySpinner.setItem(placeItem.getPlaceCategory().getId());
        categorySpinner.setOnDropDownItemClicked(position -> {
            if (priceView.getText().toString().isEmpty())
                priceView.setText(String.valueOf(list.get(position).getDefaultPrice()));
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (alertDialog != null) alertDialog.dismiss();
    }
}