package com.peter_ombodi.boomerang.android.data.model;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class ColorItem {

    private String label;
    private String id;
    private int color;

    public ColorItem(String label, String id, int color) {
        this.label = label;
        this.id = id;
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
