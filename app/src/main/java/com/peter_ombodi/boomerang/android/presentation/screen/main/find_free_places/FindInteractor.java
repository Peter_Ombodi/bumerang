package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places;

import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class FindInteractor extends BaseInteractor implements FindContract.FindModel {

    @Bean
    RoomBase roomBase;

    private IPlaceDao placeDao;

    @AfterInject
    void init() {
        placeDao = roomBase.getPlaceDao();
    }

    @Override
    public Observable<List<Integer>> getAvailablePrices() {
        return getAsyncObservable(placeDao.getAvailablePrices().toObservable());
    }
}
