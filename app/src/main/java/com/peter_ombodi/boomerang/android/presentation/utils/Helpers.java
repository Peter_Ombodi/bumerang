package com.peter_ombodi.boomerang.android.presentation.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * @author Peter Ombodi (Created on 29.11.2018).
 * Email:  p.ombodi@gmail.com
 */
public class Helpers {

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width , height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, width, height);
        Drawable bgDrawable = v.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(c);
        else
            c.drawColor(Color.WHITE);
        v.draw(c);
        return b;
    }
}
