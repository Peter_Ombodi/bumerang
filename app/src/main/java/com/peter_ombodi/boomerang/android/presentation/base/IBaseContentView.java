package com.peter_ombodi.boomerang.android.presentation.base;

import com.peter_ombodi.boomerang.android.presentation.base.content.IContentView;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IBaseContentView<T extends IBasePresenter> extends IContentView {
    void initPresenter();
    void setPresenter(T presenter);
    String getScreenName();
}
