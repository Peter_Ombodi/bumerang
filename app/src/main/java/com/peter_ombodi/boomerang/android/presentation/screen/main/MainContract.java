package com.peter_ombodi.boomerang.android.presentation.screen.main;

import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseView;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface MainContract {

    interface MainView extends IBaseView<MainPresenter> {
        void showPlacesCount(Integer cnt);
    }

    interface MainPresenter extends IBasePresenter {
        void checkPlacesCount();
    }

    interface MainModel extends IBaseModel {
        Observable<Integer> getPlacesCount();
        Completable fillPlaces();
        Completable nukeTables();
    }
}
