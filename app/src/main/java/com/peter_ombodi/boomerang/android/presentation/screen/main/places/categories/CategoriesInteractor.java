package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

@EBean
public class CategoriesInteractor extends BaseInteractor implements CategoriesContract.CategoriesModel {

    @Bean
    RoomBase roomBase;

    private IPlaceCategoryDao placeCategoryDao;


    @AfterInject
    void init() {
        placeCategoryDao = roomBase.getPlaceCategoryDao();
    }

    @Override
    public Observable<List<PlaceCategory>> getCategories() {
        return getAsyncObservable(placeCategoryDao.getAllPlaceCategories().toObservable()
                .map(tPlaceCategories -> {
                            List<PlaceCategory> responses = new ArrayList<>();
                            for (TPlaceCategory category : tPlaceCategories)
                                responses.add(category.toPlaceCategory());
                            return responses;
                        }
                ));
    }
}
