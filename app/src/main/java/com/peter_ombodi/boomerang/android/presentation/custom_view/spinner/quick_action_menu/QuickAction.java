package com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 28.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class QuickAction extends SpinnerPopupWindows {
    private View mRootView;
    private LayoutInflater mInflater;
    private ViewGroup mTrack;
    private OnActionItemClickListener mItemClickListener;
    private List<ActionItem> actionItems = new ArrayList<>();

    private int mChildPos;
    private int mInsertPos;
    private int rootHeight = 0;
    private ScrollView scrollView;
    private boolean isEditable;


    public QuickAction(Context _context, boolean _isEditable) {
        super(_context);

        mInflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setRootViewId(R.layout.spinner_dropdown_layout);
        mChildPos = 0;
        isEditable = _isEditable;
    }

    public void setRootViewId(int _id) {
        mRootView = mInflater.inflate(_id, null);
        mTrack = mRootView.findViewById(R.id.tracks);
        mRootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        scrollView = mRootView.findViewById(R.id.scrollView_SDL);
        setContentView(mRootView);
    }

    public void setOnActionItemClickListener(OnActionItemClickListener _listener) {
        mItemClickListener = _listener;
    }

    public void addActionItem(ActionItem _actionItem) {
        actionItems.add(_actionItem);

        String title = _actionItem.getTitle();
        int icon = _actionItem.getIcon();

        View container;

        container = mInflater.inflate(R.layout.spinner_dropdown_row_layout, mTrack, false);

        TextView tvTitle = container.findViewById(R.id.tvTitle_SDRL);
        ImageView ivIcon = container.findViewById(R.id.ivIcon_SDRL);
        ImageView ivArrow = container.findViewById(R.id.ivArrow_SDRL);

        final int pos = mChildPos;
        final int actionId = _actionItem.getActionId();

        if (title != null) {
            tvTitle.setText(title);
            if (!isEditable || !_actionItem.isEnabled())
                tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.textColorGreenTransparent));
            if (_actionItem.isEnabled())
                tvTitle.setOnClickListener(view -> {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(QuickAction.this, pos, actionId);
                    }

                    dismiss();
                });
        } else {
            tvTitle.setVisibility(View.GONE);
        }
        if (icon != 0) {
            ivIcon.setImageResource(icon);
            ivIcon.setColorFilter(_actionItem.isEnabled() ? _actionItem.getIconColor() : R.color.textColorGreenTransparent);
            if (_actionItem.isEnabled())
                ivIcon.setOnClickListener(view -> {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(QuickAction.this, pos, actionId);
                    }
                    dismiss();
                });
        } else ivIcon.setVisibility(View.GONE);

        if (title != null) {
            ivArrow.setOnClickListener(view -> dismiss());
        }

        mTrack.addView(container, mInsertPos);

        mChildPos++;
        mInsertPos++;
        mTrack.setEnabled(isEditable);
    }


    /**
     * Show quickaction popup. Popup is automatically positioned, on top or bottom of anchor view.
     */
    public void show(View _anchor) {

        boolean onTop = false;
        int xPos = 0, yPos = 0, arrowPos = 0;

        int[] location = new int[2];

        _anchor.getLocationOnScreen(location);

        Rect anchorRect = new Rect(location[0], location[1], location[0] + _anchor.getWidth(), location[1]
                + _anchor.getHeight());

        mRootView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mRootView.getLayoutParams().width = _anchor.getMeasuredWidth();
        mRootView.getLayoutParams().height = 200;
        rootHeight = mRootView.getMeasuredHeight();

        Display display = mWindowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int screenHeight = size.y;

        if (anchorRect.top < screenHeight / 2) {
            onTop = true;
        }
        xPos = anchorRect.left;
        yPos = calculateVerticalPosition(anchorRect, screenHeight, onTop);

        if (!onTop) mWindow.setAnimationStyle(R.style.Animations_PopUpMenu_Center);
        else mWindow.setAnimationStyle(R.style.Animations_PopDownMenu_Center);

        if (onTop && mTrack.getChildCount() > 0) {
            View arrow = mTrack.getChildAt(0).findViewById(R.id.ivArrow_SDRL);
            arrow.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) arrow.getLayoutParams();
            params.gravity = Gravity.TOP;
        } else if (!onTop && mTrack.getChildCount() > 0) {
            ImageView arrow = mTrack.getChildAt(mTrack.getChildCount() - 1).findViewById(R.id.ivArrow_SDRL);
            arrow.setVisibility(View.VISIBLE);
            arrow.setImageResource(R.drawable.ic_arrow_down);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) arrow.getLayoutParams();
            params.gravity = Gravity.BOTTOM;
        }
        if (!onTop) {
            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
        preShow(_anchor, _anchor.getMeasuredWidth(), rootHeight);
        mWindow.showAtLocation(_anchor, Gravity.NO_GRAVITY, xPos, yPos);
    }

    private int calculateVerticalPosition(Rect _anchorRect, int _screenHeight, boolean _onTop) {
        int y;

        if (!_onTop) {
            if (_anchorRect.bottom - rootHeight < 0) {
                int itemHeight = mTrack.getChildAt(0).getMeasuredHeight();
                int count = actionItems.size();
                do {
                    count--;
                    rootHeight = itemHeight * count;
                } while (_anchorRect.bottom - rootHeight < 0);
                y = _anchorRect.bottom - rootHeight;

            } else y = _anchorRect.bottom - rootHeight;
        } else {
            y = _anchorRect.top;
            if (_anchorRect.top + rootHeight > _screenHeight) {
                rootHeight = _screenHeight - _anchorRect.top;
            }
        }

        return y;
    }

    /**
     * Listener for item click
     */
    public interface OnActionItemClickListener {
        void onItemClick(QuickAction source, int pos, int actionId);
    }
}
