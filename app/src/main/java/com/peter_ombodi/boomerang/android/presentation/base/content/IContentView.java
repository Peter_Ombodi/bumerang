package com.peter_ombodi.boomerang.android.presentation.base.content;

import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

/**
 * @author Peter Ombodi (Created on 04.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IContentView {
    void showProgressMain();
    void showProgressPagination();
    void hideProgress();
    void showErrorMessage(Constants.MessageType messageType);
    void showCustomMessage(String msg, boolean isDangerous);
    void showPlaceholder(Constants.PlaceholderType placeholderType);
}
