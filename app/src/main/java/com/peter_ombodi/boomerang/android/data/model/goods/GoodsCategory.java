package com.peter_ombodi.boomerang.android.data.model.goods;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class GoodsCategory {
    private Integer id;

    private String name;

    private Integer color;
}
