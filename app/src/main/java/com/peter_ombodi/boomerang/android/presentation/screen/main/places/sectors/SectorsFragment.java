package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors;

import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.adapter.SectorsAdapter;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details.SectorDetailsFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details.SectorDetailsFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */

@EFragment(R.layout.fragment_sectors_list)
public class SectorsFragment extends MVPFragment<SectorsContract.SectorsPresenter> implements SectorsContract.SectorsView {

    @Bean
    protected SectorsInteractor interactor;

    @Bean
    SectorsAdapter adapter;

    @ViewById(R.id.rvSectors_FSL)
    RecyclerView recyclerView;

    @ViewById(R.id.etSearch_FSL)
    EditText filterView;

    @AfterInject
    @Override
    public void initPresenter() {
        new SectorsPresenter(this, interactor);
    }

    @AfterViews
    protected void initFindView() {
        if (presenter == null)
            initPresenter();
        presenter.getSectors();
        presenter.getCompositeSubscriptions()
                .add(RxTextView.textChanges(filterView)
                        .skipInitialValue() //Skip call when screen open in first time
                        .throttleLast(100, TimeUnit.MILLISECONDS)
                        .debounce(100, TimeUnit.MILLISECONDS)
                        .flatMap(charSequence -> presenter.filterSectors(charSequence.toString()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .onErrorResumeNext(Observable.empty())
                        .subscribe(queryResult -> adapter.setData(queryResult)));
    }

    @Override
    public String getScreenName() {
        return "Sectors Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_sectors_list;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSectors(List<PlaceFlat> orderHeaders) {
        adapter.setData(orderHeaders);
        adapter.setOnItemClickListener((view, item) -> presenter.onItemSelected(item));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void editSector(PlaceItem item) {
        SectorDetailsFragment fragment = SectorDetailsFragment_.builder()
                .placeItem(item)
                .build();
        fragment.setTargetFragment(getParentFragment(), Constants.CATEGORY_SAVED);
        activity.replaceFragment(fragment);
    }

}
