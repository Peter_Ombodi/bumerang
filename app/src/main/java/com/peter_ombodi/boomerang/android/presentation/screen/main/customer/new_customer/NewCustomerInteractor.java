package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.ICustomerDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TCustomer;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class NewCustomerInteractor  extends BaseInteractor implements NewCustomerContract.NewCustomerModel {

    @Bean
    RoomBase roomBase;

    private ICustomerDao customerDao;

    @AfterInject
    void init() {
        customerDao = roomBase.getCustomerDao();
    }

    @Override
    public Observable<Long> saveCustomer(Customer customer) {
        TCustomer tCustomer = TCustomer.fromCustomer(customer);
        return getAsyncObservable(Observable.fromCallable(() -> customerDao.insertCustomer(tCustomer)));
    }
}
