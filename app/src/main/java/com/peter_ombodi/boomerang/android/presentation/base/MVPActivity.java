package com.peter_ombodi.boomerang.android.presentation.base;

import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.utils.ToolbarManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EActivity
public abstract class MVPActivity <T extends IBasePresenter> extends AppCompatActivity implements IBaseView<T> {

    private boolean doubleBackToExitPressedOnce = false;

    @Bean
    protected ToolbarManager toolbarManager;

    protected T presenter;

    @IdRes
    protected abstract int getContainer();
    protected abstract Toolbar getToolbar();

    @AfterViews
    public void initToolbar() {
        toolbarManager.init(this, getToolbar());
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
                toolbarManager.showHomeButton(true);
            }
        });
    }

    @Override
    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStackImmediate();
            MVPFragment fragment = (MVPFragment) getSupportFragmentManager().findFragmentById(getContainer());
        } else {
            if (doubleBackToExitPressedOnce) {
                finish();
            } else {
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, R.string.msg_back_to_exit, Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unsubscribe();
        presenter = null;
    }

    public void replaceFirstFragment(MVPFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getContainer(), fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public void replaceFragmentClearBackstack(MVPFragment fragment) {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getContainer(), fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public ToolbarManager getToolbarManager() {
        return toolbarManager;
    }

}
