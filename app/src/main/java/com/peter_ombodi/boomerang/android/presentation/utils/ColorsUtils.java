package com.peter_ombodi.boomerang.android.presentation.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.ColorItem;

import java.util.ArrayList;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public abstract class ColorsUtils {

    private static ArrayList<ColorItem> colorItems;
    private static int defaultAppColor;

    public static ArrayList<ColorItem> getColorItems() {
        return colorItems;
    }

    public static void initEventColors(Context context) {
        defaultAppColor = ContextCompat.getColor(context, R.color.default_events_color);

        colorItems = new ArrayList<>();
        colorItems.add(new ColorItem(context.getString(R.string.dark_pink), "darkPink", ContextCompat.getColor(context, R.color.darkPink)));
        colorItems.add(new ColorItem(context.getString(R.string.pink), "pink", ContextCompat.getColor(context, R.color.pink)));
        colorItems.add(new ColorItem(context.getString(R.string.purple), "purple", ContextCompat.getColor(context, R.color.purple)));
        colorItems.add(new ColorItem(context.getString(R.string.dark_blue), "darkBlue", ContextCompat.getColor(context, R.color.darkBlue)));
        colorItems.add(new ColorItem(context.getString(R.string.blue), "blue", ContextCompat.getColor(context, R.color.blue)));
        colorItems.add(new ColorItem(context.getString(R.string.light_blue), "lightBlue", ContextCompat.getColor(context, R.color.lightBlue)));
        colorItems.add(new ColorItem(context.getString(R.string.turquoise_green), "turquoiseGreen", ContextCompat.getColor(context, R.color.turquoiseGreen)));
        colorItems.add(new ColorItem(context.getString(R.string.green), "green", ContextCompat.getColor(context, R.color.green)));
        colorItems.add(new ColorItem(context.getString(R.string.yellow), "yellow", ContextCompat.getColor(context, R.color.yellow)));
        colorItems.add(new ColorItem(context.getString(R.string.light_orange), "lightOrange", ContextCompat.getColor(context, R.color.lightOrange)));
        colorItems.add(new ColorItem(context.getString(R.string.orange), "orange", ContextCompat.getColor(context, R.color.orange)));
        colorItems.add(new ColorItem(context.getString(R.string.red), "red", ContextCompat.getColor(context, R.color.red)));
    }

    public static int getColor(String colorId) {
        for (ColorItem colorItem : colorItems) {
            if (colorItem.getId().equals(colorId)) {
                return colorItem.getColor();
            }
        }
        return defaultAppColor;
    }

    public static String getColorId(int color) {
        for (ColorItem colorItem : colorItems) {
            if (colorItem.getColor() == color) {
                return colorItem.getId();
            }
        }
        return "default";
    }
}
