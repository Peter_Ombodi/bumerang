package com.peter_ombodi.boomerang.android.presentation.custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.settings.BillSettings;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

import java.util.Calendar;

/**
 * @author Peter Ombodi (Created on 25.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

public class BillView extends View {


    private static final float LEFT_X_TITLE = 120;
    private static final float LEFT_X_SUBTITLE = 100;
    private static final float LEFT_X_CAPTION = 50;
    private static final float LEFT_X_VALUE = 320;

    private Paint paintForeground;
    private BillSettings requisites;
    private OrderHeader order;

    private int viewWidth = 1200;
    private float viewWidthScale = 1;
    private String leadingZero = "0000000000";

    private int colorTitle;
    private int colorSubTitle;
    private int colorName;
    private int colorValue;
    private int colorTotal;
    private int colorFooter;
    private int colorCanvas;

    // for visual design in IDE
    private float sizeTitle = 40;
    private float sizeSubTitle = 24;
    private float sizeName = 18;
    private float sizeValue = 22;
    private float sizeTotal = 22;
    private float sizeFooter = 22;

    private int styleTitle;
    private int styleSubTitle;
    private int styleName;
    private int styleValue;
    private int styleTotal;
    private int styleFooter;

    private float currentY = 0;

    public BillView(Context context) {
        super(context);
        init(context, null);
    }

    public BillView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BillView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BillView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                attributeSet,
                R.styleable.BillView,
                0, 0
        );
        try {
            colorTitle = styledAttributes.getInt(R.styleable.BillView_titleColor, ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            colorSubTitle = styledAttributes.getInt(R.styleable.BillView_subTitleColor, ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

            colorName = styledAttributes.getInt(R.styleable.BillView_nameColor, ContextCompat.getColor(getContext(), R.color.colorAccent));
            colorValue = styledAttributes.getInt(R.styleable.BillView_valueColor, ContextCompat.getColor(getContext(), R.color.colorPrimary));
            colorTotal = styledAttributes.getInt(R.styleable.BillView_totalColor, ContextCompat.getColor(getContext(), R.color.colorPrimary));
            colorFooter = styledAttributes.getInt(R.styleable.BillView_footerColor, ContextCompat.getColor(getContext(), R.color.colorPrimary));
            colorCanvas = styledAttributes.getInt(R.styleable.BillView_canvasColor, ContextCompat.getColor(getContext(), R.color.colorWhiteAlpha));

            sizeTitle = styledAttributes.getDimensionPixelSize(R.styleable.BillView_titleSize, 20);
            sizeSubTitle = styledAttributes.getDimensionPixelSize(R.styleable.BillView_subTitleSize, 20);
            sizeName = styledAttributes.getDimensionPixelSize(R.styleable.BillView_nameSize, 18);
            sizeValue = styledAttributes.getDimensionPixelSize(R.styleable.BillView_valueSize, 20);
            sizeTotal = styledAttributes.getDimensionPixelSize(R.styleable.BillView_titleSize, 22);
            sizeFooter = styledAttributes.getDimensionPixelSize(R.styleable.BillView_footerSize, 16);

            styleTitle = styledAttributes.getInt(R.styleable.BillView_titleStyle, Typeface.NORMAL);
            styleSubTitle = styledAttributes.getInt(R.styleable.BillView_subTitleStyle, Typeface.NORMAL);
            styleName = styledAttributes.getInt(R.styleable.BillView_nameStyle, Typeface.NORMAL);
            styleValue = styledAttributes.getInt(R.styleable.BillView_valueStyle, Typeface.BOLD);
            styleTotal = styledAttributes.getInt(R.styleable.BillView_totalStyle, Typeface.BOLD);
            styleFooter = styledAttributes.getInt(R.styleable.BillView_footerStyle, Typeface.BOLD);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // release the TypedArray so that it can be reused.
            styledAttributes.recycle();
        }
        setWillNotDraw(false);
        paintForeground = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintForeground.setStyle(Paint.Style.FILL);
        paintForeground.setAntiAlias(true);
        paintForeground.setColor(colorCanvas);
        final ViewTreeObserver observer = this.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                viewWidth = getWidth();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(colorCanvas);
        drawText(canvas, sizeTitle, styleTitle, colorTitle, requisites.getBillTitle(), true, LEFT_X_TITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, requisites.getBillSubtitle(), true, LEFT_X_SUBTITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, requisites.getAddress(), true, LEFT_X_SUBTITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, requisites.getPhone(), true, LEFT_X_SUBTITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, requisites.getEmail(), true, LEFT_X_SUBTITLE);

        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.date_arrival), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, DateManager.convertOrderDate(order.getStartDate()), false, LEFT_X_VALUE);
        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.date_leaving), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, DateManager.convertOrderDate(order.getEndDate()), false, LEFT_X_VALUE);

        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.category_name_hint), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, order.getPlaceItem().getPlaceCategory().getName(), false, LEFT_X_VALUE);
        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.sector_name), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, order.getPlaceItem().getName(), false, LEFT_X_VALUE);

        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.customer_name), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, order.getCustomer().getFirstName() + " " + order.getCustomer().getLastName(), false, LEFT_X_VALUE);

        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.customer_phone), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, order.getCustomer().getPhone(), false, LEFT_X_VALUE);

        drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.vehicle_number), true, LEFT_X_CAPTION);
        drawText(canvas, sizeValue, styleValue, colorValue, order.getCustomer().getVehicleNumber(), false, LEFT_X_VALUE);

        if (!order.getCustomer().getEmail().isEmpty()) {
            drawText(canvas, sizeName, styleName, colorName, getContext().getResources().getString(R.string.customer_email), true, LEFT_X_CAPTION);
            drawText(canvas, sizeValue, styleValue, colorValue, order.getCustomer().getEmail(), false, LEFT_X_VALUE);
        }

        drawLine(canvas, 10, colorValue, true, LEFT_X_CAPTION - 5);

        drawText(canvas, sizeTotal, styleTotal, colorName, getContext().getResources().getString(R.string.total), true, LEFT_X_CAPTION);
        drawText(canvas, sizeTotal, styleTotal, colorTotal, order.getTotal().toString(), false, LEFT_X_VALUE);

        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, "", true, LEFT_X_SUBTITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, requisites.getSlogan(), true, LEFT_X_SUBTITLE);
        drawText(canvas, sizeSubTitle, styleSubTitle, colorSubTitle, "", true, LEFT_X_SUBTITLE);

        drawText(canvas, sizeFooter, styleFooter, colorFooter, getContext().getResources().getString(R.string.time_of_create), true, LEFT_X_CAPTION);
        drawText(canvas, sizeFooter, styleFooter, colorFooter, DateManager.convertBillDate(Calendar.getInstance().getTime()), false, LEFT_X_VALUE);

        currentY = 0;
    }

    private void drawText(Canvas _canvas, float _textSize, int _textStyle, int _textColor, String _text, boolean _newLine, float _x) {
        if (_newLine) currentY = currentY + _textSize + _textSize / 2;
        paintForeground.setTextSize(_textSize);
        paintForeground.setTypeface(Typeface.defaultFromStyle(_textStyle));
        paintForeground.setColor(_textColor);
        _canvas.drawText(_text, _x*viewWidthScale, currentY, paintForeground);
    }

    private void drawLine(Canvas _canvas, float _textSize, int _color, boolean _newLine, float _x) {
        if (_newLine) currentY = currentY + _textSize + _textSize / 2;
        Paint drawPaint = new Paint();
        drawPaint.setColor(_color);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(2);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        _canvas.drawLine(_x, currentY, 600*viewWidthScale, currentY + 2, drawPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (viewWidth != 0) {
            int height = (int) ((10 * sizeValue + sizeTitle * 2 + sizeSubTitle) * 1.5 + 60);
            int width = (int) ((20 * sizeValue + sizeTitle * 20));
            setMeasuredDimension(widthMeasureSpec, height);
        } else {
            //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            // for visual design in IDE
            setMeasuredDimension(750, 750);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
//        viewHeight = h;
        viewWidth = w;
        viewWidthScale = w / 750;

        // for visual design in IDE
        if (order == null) {
            order = new OrderHeader();
            order.setStartDate(Calendar.getInstance().getTime());
            order.setEndDate(Calendar.getInstance().getTime());
            requisites = new BillSettings("Boomerang", "lake of your dreams", "+380 50 51525354", "receiption@boomerang.com", "1, Dejda pit, Beregszasz district", "You will never swim alone");
        }
    }

    public void setData(OrderHeader orderHeader, BillSettings requisites) {
        this.order = orderHeader;
        this.requisites = requisites;
        requestLayout();
        invalidate();
    }

    public Bitmap saveSignature() {
        Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        this.draw(canvas);
        return bitmap;
    }
}
