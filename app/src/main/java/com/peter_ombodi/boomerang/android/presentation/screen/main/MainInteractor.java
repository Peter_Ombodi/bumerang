package com.peter_ombodi.boomerang.android.presentation.screen.main;

import android.util.Log;

import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlace;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class MainInteractor extends BaseInteractor implements MainContract.MainModel {

    @Bean
    RoomBase roomBase;

    private IPlaceCategoryDao placeCategoryDao;
    private IPlaceDao placeDao;

    @AfterInject
    void init() {
        placeCategoryDao = roomBase.getPlaceCategoryDao();
        placeDao = roomBase.getPlaceDao();
    }

    @Override
    public Observable<Integer> getPlacesCount() {
        return getAsyncObservable(Observable.fromCallable(() -> placeDao.getPlacesCount()));
    }

    @Override
    public Completable fillPlaces() {
        List<TPlaceCategory> placeCategories = new ArrayList<>();
        List<TPlace> places = new ArrayList<>();

        for (int i = 1; i < 9; i++) {
            TPlaceCategory category = new TPlaceCategory();
            category.setName("категорія #" + String.valueOf(i));
            category.setId(i);
            category.setDefaultPrice(i * 110);
            category.setColor(i);
            placeCategories.add(category);
            for (int k = 1; k < 15; k++) {
                TPlace place = new TPlace();
                place.setCategoryId(i);
                place.setName("Сектор #" + String.valueOf((i-1) * 15 + k));
                place.setPrice(category.getDefaultPrice());
                places.add(place);
            }
            Log.d("MainInteractor", "fillPlaces category : " + category.toString());
        }

        final Completable fillCategories = getAsyncCompletable(Completable.fromAction(() -> {
            for (TPlaceCategory category : placeCategories) {
                Log.d("MainInteractor", "fillPlaces getAsyncCompletable category : " + category.toString());
                placeCategoryDao.insertCategory(category);
            }
            //placeCategoryDao.insertAll(placeCategories);
        }));
        final Completable fillPlaces = getAsyncCompletable(Completable.fromAction(() -> placeDao.insertAll(places)));
        return fillCategories.andThen(fillPlaces);
    }

    @Override
    public Completable nukeTables() {
        final Completable nukeCategories = getAsyncCompletable(Completable.fromAction(() -> placeCategoryDao.nukeTable()));
        final Completable nukePlaces = getAsyncCompletable(Completable.fromAction(() -> placeDao.nukeTable()));
        return nukeCategories.andThen(nukePlaces);
    }
}
