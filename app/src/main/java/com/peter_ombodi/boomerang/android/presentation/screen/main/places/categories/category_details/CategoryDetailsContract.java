package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 03.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public interface CategoryDetailsContract {
    interface CategoryView extends IBaseContentView<CategoryPresenter> {
        void onCategorySaved(PlaceCategory placeCategory);
        void onCategoryDeleted();
    }
    interface CategoryPresenter extends IBasePresenter {
        void onSaveClick(PlaceCategory placeCategory);
        void onDeleteClick(int categoryId);
    }
    interface CategoryModel extends IBaseModel {
        Observable<Long> saveCategory(PlaceCategory placeCategory);
        Observable<Integer> deleteCategory(int categoryId);
    }
}
