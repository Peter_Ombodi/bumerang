package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 09.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public interface SectorDetailsContract {
    interface SectorView extends IBaseContentView<SectorPresenter> {
        void onSectorSaved(PlaceItem item);
        void onSectorDeleted();
        void setCategories(List<PlaceCategory> list);
    }
    interface SectorPresenter extends IBasePresenter {
        void onSaveClick(PlaceItem item);
        void onDeleteClick(long sectorId);
        void getCategories();
    }
    interface SectorModel extends IBaseModel {
        Observable<Long> saveSector(PlaceItem item);
        Observable<Integer> deleteSector(long sectorId);
        Observable<List<PlaceCategory>> getCategories();
    }
}
