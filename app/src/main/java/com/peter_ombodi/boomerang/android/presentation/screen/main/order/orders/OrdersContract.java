package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders;

import android.support.annotation.IntDef;

import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface OrdersContract {

    @IntDef({ORDERS_ALL, ORDERS_ARRIVE, ORDERS_LEAVE})
    @Retention(RetentionPolicy.RUNTIME)
    @interface State {
    }

    int ORDERS_ALL = 0;
    int ORDERS_ARRIVE = 1;
    int ORDERS_LEAVE = 2;

    interface OrdersView extends IBaseContentView<OrdersContract.OrdersPresenter> {
        void showResult(List<OrderFlat> orderHeaders);
        void showOrderScreen(OrderFlat orderHeader);
    }

    interface OrdersPresenter extends IBasePresenter {
        void getSoldPlaces(int orderFilter);
        void getTodayArrivePlaces();
        void getTodayLeavePlaces();
        void onPlaceSelected(OrderFlat orderHeader);
        Observable<List<OrderFlat>>  filterOrders(String filter);
    }

    interface OrdersModel extends IBaseModel {
        Observable<List<OrderFlat>> getSoldPlaces(int orderFilter,String date);
        Observable<List<OrderHeader>> getTodayArrivePlaces();
        Observable<List<OrderHeader>> getTodayLeavePlaces();
    }
}
