package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlace;

import java.util.List;

import io.reactivex.Maybe;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Dao
public interface IPlaceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TPlace... places);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TPlace> places);

    @Query("DELETE FROM places")
    void nukeTable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertPlace(TPlace item);

    @Query("DELETE FROM places where id = :sectorId")
    int deletePlace(long sectorId);

    @Query("SELECT * FROM places WHERE id = :placeIds LIMIT 1")
    TPlace getPlaceById(String placeIds);

    @Query("SELECT COUNT(*) FROM places")
    Integer getPlacesCount();

    @Query("SELECT price FROM places GROUP BY price ORDER BY price")
    Maybe<List<Integer>> getAvailablePrices();

    @Query("SELECT places.*, place_categories.name as category_name, place_categories.color FROM places LEFT JOIN place_categories on places.category_id = place_categories.id order by category_id, name")
    Maybe<List<PlaceFlat>> getAllPlaces();
}
