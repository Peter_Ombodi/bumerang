package com.peter_ombodi.boomerang.android.presentation.utils;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.peter_ombodi.boomerang.android.R;

/**
 * @author Peter Ombodi (Created on 04.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class Constants {

    //public static String TIME_STAMP_FORMAT = "YYYY-MM-DD HH:MM:SS.SSS";
    public static final String TAG_CUSTOMER = "TAG_CUSTOMER";
    public static final String TAG_CATEGORY = "TAG_CATEGORY";
    public static final String TAG_SECTOR = "TAG_SECTOR";
    public static final int CUSTOMER_SELECTED = 1;
    public static final int CUSTOMER_ADDED = 2;
    public static final int BILL_SHARED = 3;
    public static final int CATEGORY_SAVED = 4;
    public static final int SECTOR_SAVED = 5;
    public static final int RESULT_SAVE_ERROR = -2;
    public static final String LOCALE_LANG_UA = "uk";
    public static final String LOCALE_REGION_UA = "UA";
    public static final String LOCALE_LANG_UK = "en";
    public static final String LOCALE_REGION_UK = "GB";
    public static final String LOCALE_LANG_HU = "hu";
    public static final String LOCALE_REGION_HU = "HU";
    public static final String PATH_BILLS = "docs/";


    public enum MessageType {
        CONNECTION_PROBLEMS(R.string.err_msg_connection_problem, true),
        UNKNOWN(R.string.err_msg_something_goes_wrong, true),
        LOADING(R.string.loading, false);

        @StringRes
        private int messageRes;
        private boolean isDangerous;

        MessageType(@StringRes int messageRes, boolean isDangerous) {
            this.messageRes = messageRes;
            this.isDangerous = isDangerous;
        }

        public int getMessageRes() {
            return messageRes;
        }

        public boolean isDangerous() {
            return isDangerous;
        }
    }

    public enum PlaceholderType {
        NETWORK(R.string.err_msg_connection_problem, R.drawable.ic_cloud_off),
        UNKNOWN(R.string.err_msg_something_goes_wrong, R.drawable.ic_sentiment_dissatisfied),
        EMPTY(R.string.err_msg_no_data, R.drawable.ic_no_data);

        @StringRes
        private int messageRes;
        @DrawableRes
        private int iconRes;

        PlaceholderType(@StringRes int messageRes, @DrawableRes int iconRes) {
            this.messageRes = messageRes;
            this.iconRes = iconRes;
        }

        public int getMessageRes() {
            return messageRes;
        }

        public int getIconRes() {
            return iconRes;
        }
    }
}
