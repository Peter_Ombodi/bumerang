package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places;

import android.util.Log;

import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class FindPresenter extends BasePresenter implements FindContract.FindPresenter {

    private FindContract.FindView view;
    private FindContract.FindModel model;

    public FindPresenter(FindContract.FindView view, FindContract.FindModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void getAvailablePrices() {
        compositeSubscriptions.add(
                model.getAvailablePrices().subscribe(prises -> {
                    Log.d("FindPresenter", "getAvailablePrices: " + prises.toString());
                    Integer[] priceArray = prises.toArray(new Integer[prises.size()]);
                    view.setAvailablePrices(priceArray);
                }, Throwable::printStackTrace));
    }
}
