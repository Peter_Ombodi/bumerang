package com.peter_ombodi.boomerang.android.data.model.order;

import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.data.model.user.AppUser;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.data.room.converters.BillDateConverter;
import com.peter_ombodi.boomerang.android.data.room.converters.ShortDateConverter;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class OrderHeader implements Serializable {

    private Integer id;

    @SerializedName("start_date")
    @TypeConverters({ShortDateConverter.class})
    private Date startDate;

    @SerializedName("end_date")
    @TypeConverters({ShortDateConverter.class})
    private Date endDate;

    @SerializedName("bill_date")
    @TypeConverters({BillDateConverter.class})
    private Date billDate;

    private String vehicle;

    private Integer status;

    private AppUser creator;

    private Customer customer;

    private Integer total;

    private PlaceItem placeItem;

    private List<OrderItem> orderItems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateConverted() {
        return DateManager.convertOrderDate(startDate);
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateConverted() {
        return DateManager.convertOrderDate(endDate);
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public AppUser getCreator() {
        return creator;
    }

    public void setCreator(AppUser creator) {
        this.creator = creator;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public PlaceItem getPlaceItem() {
        return placeItem;
    }

    public void setPlaceItem(PlaceItem placeItem) {
        this.placeItem = placeItem;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillDateConverted() {
        return DateManager.convertBillDate(billDate);
    }

}
