package com.peter_ombodi.boomerang.android.presentation.base;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IBaseView<T extends IBasePresenter> {
    void initPresenter();
    void setPresenter(T presenter);
    String getScreenName();
}
