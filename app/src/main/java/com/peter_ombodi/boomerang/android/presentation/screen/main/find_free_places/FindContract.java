package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places;

import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface FindContract {

    interface FindView extends IBaseContentView<FindPresenter> {
        void setAvailablePrices(Integer[] prices);
    }
    interface FindPresenter extends IBasePresenter {
        void getAvailablePrices();
    }
    interface FindModel extends IBaseModel {
        Observable<List<Integer>> getAvailablePrices();
    }
}
