package com.peter_ombodi.boomerang.android.data.model.place;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.TypeConverters;

import com.peter_ombodi.boomerang.android.data.room.converters.ShortDateConverter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Peter Ombodi (Created on 28.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class PlaceFree extends PlaceFlat implements Serializable{

    @ColumnInfo(name = "free_from")
    @TypeConverters({ShortDateConverter.class})
    private Date freeFrom;

    @ColumnInfo(name = "free_until")
    @TypeConverters({ShortDateConverter.class})
    private Date freeUntil;

    public Date getFreeFrom() {
        return freeFrom;
    }

    public void setFreeFrom(Date freeFrom) {
        this.freeFrom = freeFrom;
    }

    public Date getFreeUntil() {
        return freeUntil;
    }

    public void setFreeUntil(Date freeUntil) {
        this.freeUntil = freeUntil;
    }

}
