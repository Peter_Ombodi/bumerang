package com.peter_ombodi.boomerang.android.presentation.screen.main.settings;

import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public final class SettingsPresenter extends BasePresenter implements SettingsContract.SettingsPresenter {

    private SettingsContract.SettingsView view;
    private SettingsContract.SettingsModel model;

    public SettingsPresenter(SettingsContract.SettingsView view, SettingsContract.SettingsModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }
}
