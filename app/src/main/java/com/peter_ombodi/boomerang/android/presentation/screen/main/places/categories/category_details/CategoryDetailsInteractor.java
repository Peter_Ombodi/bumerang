package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.CategoryAndPlacesDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 03.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class CategoryDetailsInteractor extends BaseInteractor implements CategoryDetailsContract.CategoryModel {

    @Bean
    RoomBase roomBase;

    private IPlaceCategoryDao placeCategoryDao;
    private CategoryAndPlacesDao categoryAndPlacesDao;

    @AfterInject
    void init() {
        placeCategoryDao = roomBase.getPlaceCategoryDao();
        categoryAndPlacesDao = roomBase.getCategoryAndPlacesDao();
    }

    @Override
    public Observable<Long> saveCategory(PlaceCategory placeCategory) {
        return getAsyncObservable(Observable.fromCallable(()
                -> placeCategoryDao.insertCategory(TPlaceCategory.fromPlaceCategory(placeCategory))));
    }

    @Override
    public Observable<Integer> deleteCategory(int categoryId) {
        return getAsyncObservable(Observable.fromCallable(() ->
                categoryAndPlacesDao.deleteCategoryAndPlaces(categoryId)
        ));
    }
}