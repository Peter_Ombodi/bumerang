package com.peter_ombodi.boomerang.android.data.model.user;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class Customer extends User implements Serializable {

    private String email;
    private String phone;
    private String vehicleNumber;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public int getHash(){
        return (this.toString() + this.toHashString()).hashCode();
    }

    @Override
    public String toString() {
        return "Customer{" +
                "email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                '}';
    }
}
