package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders;

import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.adapter.OrdersAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_show_orders)
public class OrdersFragment extends MVPFragment<OrdersContract.OrdersPresenter> implements OrdersContract.OrdersView {

    @FragmentArg
    int orderFilter;

    @Bean
    protected OrdersInteractor interactor;

    @Bean
    OrdersAdapter adapter;

    @ViewById(R.id.rvOrders_FSO)
    RecyclerView recyclerView;

    @ViewById(R.id.etSearch_FSO)
    EditText filterView;

    @AfterInject
    @Override
    public void initPresenter() {
        new OrdersPresenter(this, interactor);
    }

    @AfterViews
    protected void initFindView() {
        switch (orderFilter) {
            case com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.OrdersContract.ORDERS_ALL:
                activity.getToolbarManager().setTitle(R.string.title_sold_places);
                break;
            case com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.OrdersContract.ORDERS_ARRIVE:
                activity.getToolbarManager().setTitle(R.string.title_today_arrive);
                break;
            case com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.OrdersContract.ORDERS_LEAVE:
                activity.getToolbarManager().setTitle(R.string.title_today_leave);
                break;
        }
        if (presenter == null)
            initPresenter();
        presenter.getSoldPlaces(orderFilter);
        presenter.getCompositeSubscriptions()
                .add(RxTextView.textChanges(filterView)
                        .skipInitialValue() //Skip call when screen open in first time
                        .throttleLast(100, TimeUnit.MILLISECONDS)
                        .debounce(100, TimeUnit.MILLISECONDS)
                        .flatMap(charSequence -> presenter.filterOrders(charSequence.toString()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .onErrorResumeNext(Observable.empty())
                        .subscribe(queryResult -> adapter.setData(queryResult)));
    }

    @Override
    public String getScreenName() {
        return "Orders Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_show_orders;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showResult(List<OrderFlat> orderHeaders) {
        adapter.setData(orderHeaders);
        adapter.setOnItemClickListener((view, item) -> presenter.onPlaceSelected(item));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showOrderScreen(OrderFlat orderHeader) {

    }
}