package com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details;

import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

import java.util.Calendar;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class OrderDetailsPresenter extends BasePresenter implements OrderDetailsContract.OrderDetailsPresenter {

    private OrderDetailsContract.OrderDetailsView view;
    private OrderDetailsContract.OrderDetailsModel model;

    public OrderDetailsPresenter(OrderDetailsContract.OrderDetailsView view, OrderDetailsContract.OrderDetailsModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void updateOrderInfo(OrderHeader orderHeader) {
        compositeSubscriptions
                .add(model.updateOrderInfo(orderHeader)
                        .subscribe(orderHeader1 -> {
                            view.onOrderInfoUpdated(orderHeader1);
                        }, Throwable::printStackTrace));
    }

    @Override
    public void onSaveOrderClick(OrderHeader orderHeader) {
        orderHeader.setStatus(1);
        orderHeader.setBillDate(Calendar.getInstance().getTime());
        compositeSubscriptions
                .add(model.saveOrder(orderHeader)
                        .subscribe(success -> {
                            //view.showHomeScreen();
                            view.showBill(orderHeader);
                        }, Throwable::printStackTrace));
    }

    @Override
    public void onCancelClick() {

    }

    @Override
    public void setViewData(OrderHeader orderHeader) {
        view.setCategory(orderHeader.getPlaceItem().getPlaceCategory().getName());
    }

    @Override
    public void setCustomer(Customer customer) {
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }
}
