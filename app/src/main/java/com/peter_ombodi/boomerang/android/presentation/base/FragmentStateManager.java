package com.peter_ombodi.boomerang.android.presentation.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class FragmentStateManager {

    private FragmentManager mFragmentManager;
    private ViewGroup container;
    private IFragmentSource fragmentSource;

    public void init(ViewGroup container, FragmentManager fm, IFragmentSource fragmentSource) {
        this.mFragmentManager = fm;
        this.container = container;
        this.fragmentSource = fragmentSource;
    }

    private Fragment getItem(int position) {
        return fragmentSource.getItem(position);
    }

    public Fragment changeFragment(int position) {
        FragmentTransaction fragmentTransaction = this.mFragmentManager.beginTransaction();

        for (int i = 0; i < fragmentSource.getItemCount(); i++) {
            if (i == position) continue;
            Fragment fragmentHide = findFragment(i);
            if (fragmentHide != null)
                fragmentTransaction.hide(fragmentHide);
        }

        Fragment fragment = findFragment(position);
        if (fragment == null) {
            fragment = this.getItem(position);
            fragmentTransaction.add(this.container.getId(), fragment, makeFragmentName(this.container.getId(), this.getItemId(position)));
        }
        fragmentTransaction.show(fragment);

        fragmentTransaction.commitNow();
        return fragment;
    }

    public List<Fragment> getFragments() {
        return mFragmentManager.getFragments();
    }

    public Fragment findFragment(int position) {
        String tag = makeFragmentName(this.container.getId(), this.getItemId(position));
        return this.mFragmentManager.findFragmentByTag(tag);
    }

    long getItemId(int position) {
        return (long) position;
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    public interface IFragmentSource {
        Fragment getItem(int position);
        int getItemCount();
    }
}
