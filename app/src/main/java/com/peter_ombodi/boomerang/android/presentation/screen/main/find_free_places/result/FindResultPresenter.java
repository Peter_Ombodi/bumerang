package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result;

import android.util.Log;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

import java.util.Date;

/**
 * @author Peter Ombodi (Created on 10.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class FindResultPresenter extends BasePresenter implements FindResultContract.FindResultPresenter {

    private FindResultContract.FindResultViewI view;
    private FindResultContract.FindResultModelI model;

    public FindResultPresenter(FindResultContract.FindResultViewI view, FindResultContract.FindResultModelI model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void findFreePalaces(Date startDate, Date endDate, Integer minPrice, Integer maxPrice) {
        view.showProgressMain();
        compositeSubscriptions.add(
                model.getFreePlaces(startDate, endDate, minPrice, maxPrice)
                        .subscribe(placeList -> {
                                    Log.d("FindPresenter", "findFreePalaces: placeList " + placeList.toString());
                                    view.showResult(placeList);
                                    view.hideProgress();
                                },
                                throwable -> {
                                    view.hideProgress();
                                    throwable.printStackTrace();
                                }));
    }

    @Override
    public void onPlaceSelected(PlaceFree place) {
        view.startCreateOrderScreen(place.toPlaceItem());
    }
}
