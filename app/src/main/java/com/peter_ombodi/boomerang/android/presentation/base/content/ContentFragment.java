package com.peter_ombodi.boomerang.android.presentation.base.content;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.ViewById;

/**
 * @author Peter Ombodi (Created on 04.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment
public abstract class ContentFragment extends Fragment implements IContentView {

    @SystemService
    protected InputMethodManager inputMethodManager;

    @ViewById
    protected ProgressBar pbMain_VC;
    @ViewById
    protected ProgressBar pbPagination_VC;

    @ViewById
    protected FrameLayout flContent_VC;
    @ViewById
    protected RelativeLayout rlPlaceholder_VC;

    @ViewById
    protected TextView tvPlaceholderMessage_VC;
    @ViewById
    protected ImageView ivPlaceholderImage_VC;
    @ViewById
    protected Button btnPlaceholdoerAction_VC;

    private Snackbar snackbar;

    protected abstract int getLayoutRes();

    protected abstract IBasePresenter getPresenter();

    @LayoutRes
    protected int getRootLayoutRes() {
        return R.layout.view_content;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View parent = inflater.inflate(getRootLayoutRes(), container, false);
        ViewGroup flContent = parent.findViewById(R.id.flContent_VC);
        View.inflate(getActivity(), getLayoutRes(), flContent);
        return parent;
    }

    @AfterViews
    protected void initSnackBar() {
        snackbar = Snackbar.make(flContent_VC.getChildAt(0), "", Snackbar.LENGTH_SHORT);
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);  // show multiple line
    }

//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        if (getPresenter() != null) getPresenter().unsubscribe();
//    }

    @Click(R.id.btnPlaceholdoerAction_VC)
    protected void onPlaceholderClick(){
        onPlaceholderAction();
    }

    protected void onPlaceholderAction() {
        getPresenter().subscribe();
    }

    @Override
    public void showErrorMessage(Constants.MessageType messageType) {
        showCustomMessage(getString(messageType.getMessageRes()), messageType.isDangerous());
    }

    @Override
    public void showCustomMessage(String msg, boolean isDangerous) {
        hideProgress();
        if (snackbar.isShown()) snackbar.dismiss();
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), isDangerous
                ? R.color.colorRed
                : R.color.colorPrimary));
        snackbar.setText(msg);
        snackbar.show();
    }

    @Override
    public void showPlaceholder(Constants.PlaceholderType placeholderType) {
//        hideKeyboard();
        dismissUI();
        rlPlaceholder_VC.setVisibility(View.VISIBLE);
        ivPlaceholderImage_VC.setImageResource(placeholderType.getIconRes());
        tvPlaceholderMessage_VC.setText(placeholderType.getMessageRes());
        btnPlaceholdoerAction_VC.setVisibility(placeholderType == Constants.PlaceholderType.EMPTY
                ? View.GONE
                : View.VISIBLE);
    }

    @Override
    public void showProgressMain() {
        hideKeyboard();
        dismissUI();
        pbMain_VC.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressPagination() {
        pbPagination_VC.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbMain_VC.setVisibility(View.GONE);
        pbPagination_VC.setVisibility(View.GONE);
        flContent_VC.setVisibility(View.VISIBLE);
        rlPlaceholder_VC.setVisibility(View.GONE);
    }

    private void dismissUI() {
        pbMain_VC.setVisibility(View.GONE);
        pbPagination_VC.setVisibility(View.GONE);
        flContent_VC.setVisibility(View.GONE);
        rlPlaceholder_VC.setVisibility(View.GONE);
    }

    public boolean isSnackbarShown() {
        if (snackbar != null)
            return snackbar.isShown();
        else
            return false;
    }

    protected void hideKeyboard() {
        if(getView() != null) {
            inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }
}
