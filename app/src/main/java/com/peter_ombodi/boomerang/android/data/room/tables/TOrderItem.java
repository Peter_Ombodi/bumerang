package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.peter_ombodi.boomerang.android.data.room.converters.ShortDateConverter;

import java.util.Date;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "order_items")
public class TOrderItem {
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "add_time")
    @TypeConverters({ShortDateConverter.class})
    private Date addTime;

    @ColumnInfo(name = "edit_time")
    @TypeConverters({ShortDateConverter.class})
    private Date editTime;

    private Integer status;

    @ColumnInfo(name = "add_user_id")
    private Integer addUserId;

    @ColumnInfo(name = "edit_user_id")
    private Integer editUserId;

    private Integer price;

    private Integer quantity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAddUserId() {
        return addUserId;
    }

    public void setAddUserId(Integer addUserId) {
        this.addUserId = addUserId;
    }

    public Integer getEditUserId() {
        return editUserId;
    }

    public void setEditUserId(Integer editUserId) {
        this.editUserId = editUserId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
