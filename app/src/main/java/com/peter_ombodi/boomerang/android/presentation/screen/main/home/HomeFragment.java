package com.peter_ombodi.boomerang.android.presentation.screen.main.home;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.MainActivity;
import com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.FindFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.home.HomeContract.HomeView;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.OrdersContract;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders.OrdersFragment_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.res.StringRes;

import java.util.Objects;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

@EFragment(R.layout.fragment_home)
public class HomeFragment extends MVPFragment<HomeContract.HomePresenter> implements HomeView {

    @Bean
    protected HomeInteractor interactor;

    @StringRes(R.string.title_home)
    protected String toolbarTitle;

    @AfterInject
    @Override
    public void initPresenter() {
        new HomePresenter(this, interactor);
    }

    @AfterViews
    protected void initHomeView() {
        activity.getToolbarManager().setTitle(toolbarTitle);
        ((MainActivity) Objects.requireNonNull(getActivity())).updateNavigationItem(R.id.menuHome,true);
    }

    @Click(R.id.btnFind_HF)
    void onFindClick(){
        activity.replaceFragment(FindFragment_.builder().build());
    }

    @Click(R.id.btnSold_HF)
    void onSoldClick(){
        activity.replaceFragment(OrdersFragment_.builder()
                .orderFilter(OrdersContract.ORDERS_ALL)
                .build());
    }

    @Click(R.id.btnTodayArrive_HF)
    void onArriveClick(){
        activity.replaceFragment(OrdersFragment_.builder()
                .orderFilter(OrdersContract.ORDERS_ARRIVE)
                .build());
    }

    @Click(R.id.btnTodeyLeave_HF)
    void onLeaveClick(){
        activity.replaceFragment(OrdersFragment_.builder()
                .orderFilter(OrdersContract.ORDERS_LEAVE)
                .build());
    }

    @Override
    public String getScreenName() {
        return "HomeFragment Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }
}
