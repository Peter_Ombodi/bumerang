package com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 * @author Peter Ombodi (Created on 28.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class SpinnerPopupWindows {

    protected Context mContext;
    protected PopupWindow mWindow;
    protected View mRootView;
    protected Drawable mBackground = null;
    protected WindowManager mWindowManager;

    /**
     * Constructor.
     *
     * @param _context Context
     */
    @SuppressLint("ClickableViewAccessibility")
    public SpinnerPopupWindows(Context _context) {
        mContext = _context;
        mWindow = new PopupWindow(_context);
        mWindow.setTouchInterceptor((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                mWindow.dismiss();
                return true;
            }
            return false;
        });
        mWindowManager = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
    }

    /**
     * On show
     */
    protected void onShow() {
    }

    /**
     * On pre show
     */
    protected void preShow(View _anchor, int _width, int _height) {
        if (mRootView == null) throw new IllegalStateException("setContentView was not called with a view to display.");

        onShow();

        if (mBackground == null) mWindow.setBackgroundDrawable(new BitmapDrawable());
        else mWindow.setBackgroundDrawable(mBackground);

        mWindow.setWidth(_width);
        mWindow.setHeight(_height);
        mWindow.setTouchable(true);
        mWindow.setFocusable(true);
        mWindow.setOutsideTouchable(true);
        mWindow.setElevation(24);
        mWindow.setContentView(mRootView);
    }

    /**
     * Set content view.
     *
     * @param _root Root view
     */
    public void setContentView(View _root) {
        mRootView = _root;

        mWindow.setContentView(_root);
    }

    /**
     * Dismiss the popup window.
     */
    public void dismiss() {
        mWindow.dismiss();
    }
}
