package com.peter_ombodi.boomerang.android.data.model.place;

import android.arch.persistence.room.ColumnInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public class PlaceFlat implements Serializable {

    private Long id;

    @ColumnInfo(name = "category_id")
    @SerializedName("category_id")
    private Integer categoryId;

    private String name;

    @ColumnInfo(name = "category_name")
    @SerializedName("category_name")
    private String categoryName;

    private Integer price;

    @ColumnInfo(name = "color")
    @SerializedName("color_id")
    private Integer colorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public PlaceItem toPlaceItem() {
        final PlaceItem convertedPlaceItem = new PlaceItem();
        convertedPlaceItem.setId(getId());
        convertedPlaceItem.setName(getName());
        convertedPlaceItem.setPrice(getPrice());
        PlaceCategory  category = new PlaceCategory();
        category.setName(getCategoryName());
        category.setId(getCategoryId());
        convertedPlaceItem.setPlaceCategory(category);
        return convertedPlaceItem;
    }
}
