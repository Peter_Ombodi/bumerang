package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

/**
 * @author Peter Ombodi (Created on 28.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@Dao
public abstract class CategoryAndPlacesDao {

    @Query("DELETE FROM place_categories where id = :categoryId")
    abstract int deleteCategory(int categoryId);

    @Query("DELETE FROM places where category_id = :categoryId")
    abstract void deletePlace(long categoryId);

    @Transaction
    public int deleteCategoryAndPlaces(int categoryId) {
        deletePlace(categoryId);
        return deleteCategory(categoryId);
    }
}
