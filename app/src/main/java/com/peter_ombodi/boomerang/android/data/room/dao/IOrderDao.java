package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.data.room.tables.TOrder;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlace;

import java.util.List;

import io.reactivex.Maybe;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Dao
public interface IOrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TOrder... orders);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrder(TOrder order);

    @Query("DELETE FROM orders")
    void nukeTable();

    @Query("SELECT * FROM orders WHERE id = :orderId LIMIT 1")
    TOrder getOrderById(String orderId);

    @Query("SELECT * FROM orders WHERE place_id = :placeId AND date(start_date)= :from AND date(end_date)= :to LIMIT 1")
    Maybe<TOrder> getOrderInfo(String from, String to, String placeId);

    @Query("SELECT places.*, place_categories.name as category_name, place_categories.color, " +
            "(select max(o2.end_date) from orders as o2 where o2.place_id = places.id  AND (:from > date(o2.end_date))) as free_from, " +
            "(select min(o3.start_date) from orders as o3 where o3.place_id = places.id  AND (:to < date(o3.start_date))) as free_until " +
            "FROM places " +
            "LEFT JOIN orders  as o1 ON places.id = o1.place_id AND ((:from BETWEEN date(o1.start_date) AND date(o1.end_date)) OR (:to BETWEEN date(o1.start_date) AND date(o1.end_date)))" +
            "LEFT JOIN place_categories ON places.category_id = place_categories.id " +
            "WHERE o1.id IS NULL AND price BETWEEN :minPrice AND :maxPrice ORDER BY places.id")
    Maybe<List<PlaceFree>> getFreePlaces(String from, String to, Integer minPrice, Integer maxPrice);

    @Query("SELECT places.*, orders.id FROM places LEFT JOIN orders ON places.id = orders.place_id WHERE orders.id IS NULL")
    Maybe<List<TPlace>> getFreePlaces();

    @Query("SELECT places.name as place_name,place_categories.color, orders.*, customers.* FROM orders " +
            "LEFT JOIN places ON orders.place_id = places.id " +
            "LEFT JOIN place_categories ON places.category_id = place_categories.id " +
            "LEFT JOIN customers ON orders.customer_id = customers.id " +
            "WHERE orders.status = 1 ORDER BY orders.start_date, orders.place_id")
    Maybe<List<OrderFlat>> getSoldPlaces();

    @Query("SELECT places.name as place_name,place_categories.color, orders.*, customers.* FROM orders " +
            "LEFT JOIN places ON orders.place_id = places.id " +
            "LEFT JOIN place_categories ON places.category_id = place_categories.id " +
            "LEFT JOIN customers ON orders.customer_id = customers.id " +
            "WHERE orders.status = 1 AND date(start_date)=:arriveDate ORDER BY orders.place_id")
    Maybe<List<OrderFlat>> getArrivePlaces(String arriveDate);

    @Query("SELECT places.name as place_name,place_categories.color as color, orders.*, customers.* FROM orders " +
            "LEFT JOIN places ON orders.place_id = places.id " +
            "LEFT JOIN place_categories ON places.category_id = place_categories.id " +
            "LEFT JOIN customers ON orders.customer_id = customers.id " +
            "WHERE orders.status = 1 AND date(end_date)=:leaveDate ORDER BY orders.place_id")
    Maybe<List<OrderFlat>> getLeavePlaces(String leaveDate);

}
