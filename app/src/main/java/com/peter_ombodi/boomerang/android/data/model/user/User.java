package com.peter_ombodi.boomerang.android.data.model.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public abstract class User implements Serializable {

    private long id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("is_active")
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String toHashString(){
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName +
                '}';
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
