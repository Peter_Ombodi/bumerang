package com.peter_ombodi.boomerang.android.data.room.tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Entity(tableName = "customers")
public class TCustomer {
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    private String email;
    private String phone;

    @ColumnInfo(name = "vehicle_number")
    private String vehicleNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    @Override
    public String toString() {
        return "TCustomer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                '}';
    }

    public static TCustomer fromCustomer(Customer customer) {
        final TCustomer tCustomer = new TCustomer();
        tCustomer.setEmail(customer.getEmail());
        tCustomer.setFirstName(customer.getFirstName());
        tCustomer.setLastName(customer.getLastName());
        tCustomer.setPhone(customer.getPhone());
        tCustomer.setVehicleNumber(customer.getVehicleNumber());
        return tCustomer;
    }

    public static Customer fromTCustomer(TCustomer tCustomer) {
        final Customer customer = new Customer();
        customer.setId(tCustomer.getId());
        customer.setEmail(tCustomer.getEmail());
        customer.setFirstName(tCustomer.getFirstName());
        customer.setLastName(tCustomer.getLastName());
        customer.setPhone(tCustomer.getPhone());
        customer.setVehicleNumber(tCustomer.getVehicleNumber());
        return customer;
    }
}
