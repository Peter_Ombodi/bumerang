package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface NewCustomerContract {
    interface NewCustomerView extends IBaseContentView<NewCustomerPresenter> {
        void onCustomerSaved(Customer customer);
    }
    interface NewCustomerPresenter extends IBasePresenter {
        void onSaveClick(Customer customer);
    }
    interface NewCustomerModel extends IBaseModel {
        Observable<Long> saveCustomer(Customer customer);
    }
}
