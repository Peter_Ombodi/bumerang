package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.CustomSpinner;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.ActionItem;
import com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.FindContract.FindView;
import com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result.FindResultFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result.FindResultFragment_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.cleverpumpkin.calendar.CalendarDate;
import ru.cleverpumpkin.calendar.CalendarView;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

@EFragment(R.layout.fragment_find)
public class FindFragment extends MVPFragment<FindContract.FindPresenter> implements FindView {


    @ViewById(R.id.calendarFind_FF)
    CalendarView calendarView;
    @ViewById(R.id.spMaxPrice_FF)
    CustomSpinner maxPriceView;
    @ViewById(R.id.spMinPrice_FF)
    CustomSpinner minPriceView;

    @Bean
    protected FindInteractor interactor;

    private Date findStartDate;
    private Date findEndDate;

    @AfterInject
    @Override
    public void initPresenter() {
        new FindPresenter(this, interactor);
    }

    @AfterViews
    protected void initFindView() {
        activity.getToolbarManager().setTitle(R.string.title_find_a_free_places);
        if (presenter == null)
            initPresenter();
        Calendar calendar = Calendar.getInstance();
        CalendarDate calendarDate = new CalendarDate(calendar.getTime());
        calendarView.setupCalendar(calendarDate, calendarDate, null, CalendarView.SelectionMode.RANGE, new ArrayList<>());

        presenter.getAvailablePrices();
    }

    @Click(R.id.btnFind_FF)
    void onFindClick() {
        List<CalendarDate> calendarDates = calendarView.getSelectedDates();
        if (calendarDates.size() == 0) {
            findStartDate = Calendar.getInstance().getTime();
            findEndDate = new Date(findStartDate.getTime() + (1000 * 60 * 60 * 24));
        } else {
            findStartDate = calendarDates.get(0).getDate();
            findEndDate = new Date(calendarDates.get(calendarDates.size() - 1).getDate().getTime() + (1000 * 60 * 60 * 24));
        }
        FindResultFragment fragment = FindResultFragment_.builder()
                .findStartDate(findStartDate)
                .findEndDate(findEndDate)
                .minPrice(Integer.valueOf(minPriceView.getTitle()))
                .maxPrice(Integer.valueOf(maxPriceView.getTitle()))
                .build();
        activity.replaceFragment(fragment);
    }

    @Override
    public String getScreenName() {
        return "FindFragment Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_find;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setAvailablePrices(Integer[] prices) {

        List<ActionItem> spinnerMaxItems = new ArrayList<>();
        int i = 0;
        for (Integer price : prices) {
            spinnerMaxItems.add(new ActionItem(i++, String.valueOf(price), 0));
        }
        minPriceView.setDropDownItems(spinnerMaxItems);
        maxPriceView.setDropDownItems(spinnerMaxItems);
        minPriceView.setItem(0);
        maxPriceView.setItem(prices.length - 1);
        minPriceView.setOnDropDownItemClicked(view -> {
            if (minPriceView.getIdItem() > maxPriceView.getIdItem())
                maxPriceView.setItem(minPriceView.getIdItem());
        });
        maxPriceView.setOnDropDownItemClicked(view -> {
            if (maxPriceView.getIdItem() < minPriceView.getIdItem())
                minPriceView.setItem(maxPriceView.getIdItem());
        });
    }
}
