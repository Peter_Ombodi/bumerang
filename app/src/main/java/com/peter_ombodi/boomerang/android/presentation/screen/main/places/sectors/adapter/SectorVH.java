package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */

public final class SectorVH extends RecyclerView.ViewHolder implements IProviderItemClickListener<PlaceFlat> {

    private TextView sectorNameView;
    private TextView categoryNameView;
    private TextView priceView;
    private View markerView;


    public SectorVH(View itemView) {
        super(itemView);
        sectorNameView = itemView.findViewById(R.id.tvPlaceName_VIS);
        categoryNameView = itemView.findViewById(R.id.tvCategoryName_VIS);
        priceView = itemView.findViewById(R.id.tvPrice_VIS);
        markerView = itemView.findViewById(R.id.vMarker_VIS);
    }

    public void bind(PlaceFlat item) {
        itemView.setTag(item);
        sectorNameView.setText(item.getName());
        categoryNameView.setText(item.getCategoryName());
        markerView.setBackgroundColor(ColorsUtils.getColorItems().get(item.getColorId()).getColor());
        priceView.setText(String.valueOf(item.getPrice()));
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<PlaceFlat> listener) {
        itemView.setOnClickListener(view -> listener.onClick(view, (PlaceFlat) view.getTag()));
    }
}
