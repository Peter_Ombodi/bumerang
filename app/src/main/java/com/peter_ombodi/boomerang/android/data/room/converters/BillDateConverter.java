package com.peter_ombodi.boomerang.android.data.room.converters;

import android.arch.persistence.room.TypeConverter;

import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Peter Ombodi (Created on 31.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class BillDateConverter {

    private static DateFormat df = new SimpleDateFormat(DateManager.DATE_FORMAT_BILL, Locale.US);

    @TypeConverter
    public static Date fromTimestamp(String value) {
        if (value != null) {
            try {
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    @TypeConverter
    public static String toTimestamp(Date value) {
        return DateManager.convertBillDate(value);
    }
}
