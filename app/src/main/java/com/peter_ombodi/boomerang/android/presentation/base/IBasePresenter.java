package com.peter_ombodi.boomerang.android.presentation.base;

import io.reactivex.disposables.CompositeDisposable;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IBasePresenter {
    void subscribe();
    void unsubscribe();
    CompositeDisposable getCompositeSubscriptions();
}
