package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class FreePlaceAdapter extends RecyclerView.Adapter<FreePlaceVH> implements IProviderItemClickListener<PlaceFree> {

    private IItemClickListener<PlaceFree> clickListener;
    private List<PlaceFree> PlaceFrees;

    public void setData(List<PlaceFree> list) {
        PlaceFrees = list;
        notifyDataSetChanged();
    }

    public void clear() {
        if (PlaceFrees != null) {
            PlaceFrees.clear();
            notifyDataSetChanged();
        }
    }

    public void notifySlot(PlaceFree item) {
        notifyItemChanged(PlaceFrees.indexOf(item));
    }

    @NonNull
    @Override
    public FreePlaceVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FreePlaceVH vh = new FreePlaceVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_sector_free, parent, false));
        vh.setOnItemClickListener(clickListener);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull FreePlaceVH holder, int position) {
        holder.bind(PlaceFrees.get(position));
    }

    @Override
    public int getItemCount() {
        return PlaceFrees == null ? 0 : PlaceFrees.size();
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<PlaceFree> listener) {
        this.clickListener = listener;
    }
}
