package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface CategoriesContract {

    interface CategoriesView extends IBaseContentView<CategoriesPresenter> {
        void showCategories(List<PlaceCategory> list);
        void editCategory(PlaceCategory placeCategory);
    }

    interface CategoriesPresenter extends IBasePresenter {
        void getCategories();
        void onItemSelected(PlaceCategory placeCategory);
    }

    interface CategoriesModel extends IBaseModel {
        Observable<List<PlaceCategory>> getCategories();
    }
}
