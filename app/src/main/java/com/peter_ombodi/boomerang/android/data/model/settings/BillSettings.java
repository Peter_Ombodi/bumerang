package com.peter_ombodi.boomerang.android.data.model.settings;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public class BillSettings {
    private String billTitle;
    private String billSubtitle;
    private String address;
    private String phone;
    private String email;
    private String slogan;

    public BillSettings() {
    }

    public BillSettings(String billTitle, String billSubtitle, String address, String phone, String email, String slogan) {
        this.billTitle = billTitle;
        this.billSubtitle = billSubtitle;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.slogan = slogan;
    }

    public String getBillTitle() {
        return billTitle;
    }

    public void setBillTitle(String billTitle) {
        this.billTitle = billTitle;
    }

    public String getBillSubtitle() {
        return billSubtitle;
    }

    public void setBillSubtitle(String billSubtitle) {
        this.billSubtitle = billSubtitle;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Override
    public String toString() {
        return "BillSettings{" +
                "billTitle='" + billTitle + '\'' +
                ", billSubtitle='" + billSubtitle + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", slogan='" + slogan + '\'' +
                '}';
    }
}
