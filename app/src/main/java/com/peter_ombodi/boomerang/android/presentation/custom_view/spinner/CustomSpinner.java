package com.peter_ombodi.boomerang.android.presentation.custom_view.spinner;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.ActionItem;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.QuickAction;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 28.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class CustomSpinner extends android.support.v7.widget.AppCompatTextView {
    private OnItemClickListener listener;
    private List<? extends ActionItem> mItems;
    private int idItem;
    private String title;
    private boolean isEditable = true;
    private int iconRight = 32;
    private int iconBottom = 24;

    public CustomSpinner(Context _context) {
        this(_context, null);
    }

    public CustomSpinner(Context _context, AttributeSet _attrs) {
        this(_context, _attrs, 0);
    }

    public CustomSpinner(Context _context, AttributeSet _attrs, int _defStyleAttr) {
        super(_context, _attrs, _defStyleAttr);
        setOnClickListener(this::showDropDown);
    }

    public void setDropDownItems(List<? extends ActionItem> _items) {
        mItems = _items;
    }

    public void setOnDropDownItemClicked(OnItemClickListener _listener) {
        listener = _listener;
    }

    private void showDropDown(View _anchor) {
        final QuickAction quickAction = new QuickAction(getContext(), isEditable);
        if (mItems != null) {
            for (int i = 0; i < mItems.size(); i++) {
                quickAction.addActionItem(mItems.get(i));
            }
            if (isEditable)
                quickAction.setOnActionItemClickListener((source, pos, actionId) -> onItemClicked(pos));
            quickAction.show(_anchor);
        }
    }

    public interface OnItemClickListener {
        void onClicked(int _position);
    }

    public void setEditable(boolean _isEditable) {
        this.isEditable = _isEditable;
    }

    private void onItemClicked(int _position) {
        ActionItem actionItem = mItems.get(_position);
        idItem = actionItem.getActionId();
        title = actionItem.getTitle();
        setText(actionItem.getTitle());
        setItemIcon(actionItem);
        if (listener != null) {
            listener.onClicked(_position);
        }
    }

    private void setItemIcon(ActionItem actionItem) {
        if (actionItem.getIcon() != 0) {
            Drawable img = getContext().getResources().getDrawable(actionItem.getIcon());
            img.setBounds(0, 0, iconRight, iconBottom);
            if (actionItem.getIconColor() != 0)
                img.setColorFilter(actionItem.getIconColor(), PorterDuff.Mode.SRC_IN);
            setCompoundDrawablePadding(10);
            setCompoundDrawables(img, null, null, null);
        }
    }

    public int getIdItem() {
        return idItem;
    }

    public String getTitle() {
        return title;
    }

    public void setItem(int _id) {
        for (ActionItem item : mItems) {
            if (item.getActionId() == _id) {
                idItem = item.getActionId();
                title = item.getTitle();
                setText(item.getTitle());
                setItemIcon(item);
                break;
            }
        }
    }

    public void clearSpinner() {
        title = "";
        idItem = -1;
        setText("");
    }

    public void setIconRight(int iconRight) {
        this.iconRight = iconRight;
    }

    public void setIconBottom(int iconBottom) {
        this.iconBottom = iconBottom;
    }

}
