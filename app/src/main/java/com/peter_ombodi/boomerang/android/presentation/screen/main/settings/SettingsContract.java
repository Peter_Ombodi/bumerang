package com.peter_ombodi.boomerang.android.presentation.screen.main.settings;

import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public interface SettingsContract {

    interface SettingsView extends IBaseContentView<SettingsPresenter> {
    }
    interface SettingsPresenter extends IBasePresenter {
    }
    interface SettingsModel extends IBaseModel {
    }
}
