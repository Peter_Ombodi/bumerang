package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseContentView;
import com.peter_ombodi.boomerang.android.presentation.base.IBaseModel;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 10.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface FindResultContract {
    interface FindResultViewI extends IBaseContentView<FindResultContract.FindResultPresenter> {
        void showResult(List<PlaceFree> placeItems);
        void startCreateOrderScreen(PlaceItem place);
    }

    interface FindResultPresenter extends IBasePresenter {
        void findFreePalaces(Date startDate, Date endDate, Integer minPrice, Integer maxPrice);
        void onPlaceSelected(PlaceFree place);
    }

    interface FindResultModelI extends IBaseModel {
        Observable<List<PlaceFree>> getFreePlaces(Date startDate, Date endDate, Integer minPrice, Integer maxPrice);
    }

}
