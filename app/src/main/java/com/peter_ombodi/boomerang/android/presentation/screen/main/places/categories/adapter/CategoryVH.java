package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.utils.ColorsUtils;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class CategoryVH extends RecyclerView.ViewHolder implements IProviderItemClickListener<PlaceCategory> {

    private View markerView;
    private TextView nameView;
    private TextView priceView;

    public CategoryVH(View itemView) {
        super(itemView);
        markerView = itemView.findViewById(R.id.vMarker_VIC);
        nameView = itemView.findViewById(R.id.tvName_VIC);
        priceView = itemView.findViewById(R.id.tvPrice_VIC);
    }

    public void bind(PlaceCategory item) {
        itemView.setTag(item);
        nameView.setText(item.getName());
        priceView.setText(String.valueOf(item.getDefaultPrice()));
        markerView.setBackgroundColor(ColorsUtils.getColorItems().get(item.getColor()).getColor());
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<PlaceCategory> listener) {
        itemView.setOnClickListener(view -> listener.onClick(view, (PlaceCategory) view.getTag()));
    }
}
