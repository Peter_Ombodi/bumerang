package com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result;

import android.support.v7.widget.RecyclerView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFree;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.find_free_places.result.adapter.FreePlaceAdapter;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsContract;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.Date;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 10.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_show_find_result)
public class FindResultFragment extends MVPFragment<FindResultContract.FindResultPresenter> implements FindResultContract.FindResultViewI {

    @FragmentArg
    Date findStartDate;
    @FragmentArg
    Date findEndDate;
    @FragmentArg
    Integer minPrice;
    @FragmentArg
    Integer maxPrice;

    @Bean
    protected FindResultInteractor interactor;
    @Bean
    FreePlaceAdapter adapter;

    @ViewById(R.id.rvFreePlaces_FSFR)
    RecyclerView recyclerView;

    @AfterInject
    @Override
    public void initPresenter() {
        new FindResultPresenter(this, interactor);
    }

    @AfterViews
    protected void initFindView() {
        activity.getToolbarManager().setTitle(R.string.title_available_free_places);
        if (presenter == null)
            initPresenter();
        presenter.findFreePalaces(findStartDate, findEndDate, minPrice, maxPrice);
    }

    @Override
    public String getScreenName() {
        return "FindResultFragment Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_show_find_result;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showResult(List<PlaceFree> placeItems) {
        adapter.setData(placeItems);
        adapter.setOnItemClickListener((view, item) -> presenter.onPlaceSelected(item));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void startCreateOrderScreen(PlaceItem place) {
        OrderHeader orderHeader = new OrderHeader();
        orderHeader.setStartDate(findStartDate);
        orderHeader.setEndDate(findEndDate);
        orderHeader.setPlaceItem(place);
        orderHeader.setStatus(OrderDetailsContract.ORDER_NEW);
        orderHeader.setTotal(place.getPrice() * (DateManager.differenceOfDates(findStartDate, findEndDate)));
        OrderDetailsFragment fragment = OrderDetailsFragment_.builder()
                .orderHeader(orderHeader)
                .build();
        activity.replaceFragment(fragment);
    }
}
