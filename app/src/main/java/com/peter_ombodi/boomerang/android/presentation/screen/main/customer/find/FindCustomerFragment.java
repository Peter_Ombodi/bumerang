package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find.adapter.CustomerAdapter;
import com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer.NewCustomerFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer.NewCustomerFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_find_customer)
public class FindCustomerFragment extends MVPFragment<FindCustomerContract.FindCustomerPresenter> implements FindCustomerContract.FindCustomerView {

    @ViewById(R.id.etSearch_FFC)
    EditText filterView;

    @ViewById(R.id.rvCustomers_FFC)
    RecyclerView recyclerView;

//    @ViewById(R.id.srlRefreshContacts_FFC)
//    SwipeRefreshLayout refreshView;

    @Bean
    protected FindCustomerInteractor interactor;

    @Bean
    CustomerAdapter adapter;

    @AfterInject
    @Override
    public void initPresenter() {
        new FindCustomerPresenter(this, interactor);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(R.string.title_find_customer);
        if (presenter == null)
            initPresenter();
        presenter.getAllCustomers();
        presenter.getCompositeSubscriptions()
                .add(RxTextView.textChanges(filterView)
                        .skipInitialValue() //Skip call when screen open in first time
                        .throttleLast(100, TimeUnit.MILLISECONDS)
                        .debounce(100, TimeUnit.MILLISECONDS)
                        .flatMap(charSequence -> presenter.filterCustomers(charSequence.toString()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .onErrorResumeNext(Observable.empty())
                        .subscribe(queryResult -> adapter.setData(queryResult)));
    }

    @Override
    public String getScreenName() {
        return "FindCustomerFragment";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_find_customer;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showResult(List<Customer> items) {
        adapter.setData(items);
        adapter.setOnItemClickListener((view, item) -> presenter.onCustomerSelected(item));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void selectCustomer(Customer customer) {
        hideKeyboard();
        final Intent intentResult = new Intent();
        intentResult.putExtra(Constants.TAG_CUSTOMER, customer);
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(Constants.CUSTOMER_SELECTED, Activity.RESULT_OK, intentResult);
        }
        activity.onBackPressed();
    }

    @Click(R.id.btnNewCustomer_FFC)
    void onNewCustomerClick() {
        NewCustomerFragment fragment = NewCustomerFragment_.builder()
                .build();
        fragment.setTargetFragment(this, Constants.CUSTOMER_SELECTED);
        activity.replaceFragment(fragment);
    }

    @OnActivityResult(Constants.CUSTOMER_ADDED)
    protected void onNewCustomer(int resultCode, @OnActivityResult.Extra(Constants.TAG_CUSTOMER) Customer customer) {
        if (resultCode == Activity.RESULT_OK) {
            selectCustomer(customer);
        }
    }
}
