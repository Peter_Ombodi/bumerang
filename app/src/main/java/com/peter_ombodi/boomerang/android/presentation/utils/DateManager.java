package com.peter_ombodi.boomerang.android.presentation.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * @author Peter Ombodi (Created on 05.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public abstract class DateManager {

    public static final String TIME_STAMP_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_READABLE = "dd MMM yyyy, EEE";
    public static final String DATE_FORMAT_SHORT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_BILL = "dd.MM.yyyy, HH:mm";

    private static final SimpleDateFormat dateDayPickerFormat = new SimpleDateFormat(TIME_STAMP_FORMAT, Locale.getDefault());
    private static final SimpleDateFormat dateDayOrderFormat = new SimpleDateFormat(DATE_FORMAT_READABLE, Locale.getDefault());
    private static final SimpleDateFormat dateDayShortFormat = new SimpleDateFormat(DATE_FORMAT_SHORT, Locale.getDefault());
    private static final SimpleDateFormat dateBillFormat = new SimpleDateFormat(DATE_FORMAT_BILL, Locale.getDefault());

    public static String convertPickerDate(Date time) {
        return dateDayPickerFormat.format(time);
    }

    public static String convertOrderDate(Date time) {
        if (time == null) return "";
        return dateDayOrderFormat.format(time);
    }

    public static String convertBillDate(Date time) {
        return dateBillFormat.format(time);
    }

    public static String convertShortDate(Date time) {
        return dateDayShortFormat.format(time);
    }

    public static int differenceOfDates(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return (int) (diff / (24 * 60 * 60 * 1000));
    }

}
