package com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find.FindCustomerFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find.FindCustomerFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.home.HomeFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.bill.ShareBillFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.order.bill.ShareBillFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;
import com.peter_ombodi.boomerang.android.presentation.utils.DateManager;
import com.peter_ombodi.boomerang.android.presentation.utils.Helpers;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Peter Ombodi (Created on 11.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_order_details)
public class OrderDetailsFragment extends MVPFragment<OrderDetailsContract.OrderDetailsPresenter> implements OrderDetailsContract.OrderDetailsView {

    private Customer customer;

    @ViewById(R.id.tvDateStart_FNO)
    TextInputEditText startDateView;
    @ViewById(R.id.tvDateEnd_FNO)
    TextInputEditText endDateView;

    @ViewById(R.id.tvCategoryName_FNO)
    TextInputEditText categoryView;
    @ViewById(R.id.tvSectorName_FNO)
    TextInputEditText sectorView;
    @ViewById(R.id.tvFirstName_FNO)
    TextInputEditText firstNameView;
    @ViewById(R.id.tvLastName_FNO)
    TextInputEditText lastNameView;
    @ViewById(R.id.tvPhone_FNO)
    TextInputEditText phoneView;
    @ViewById(R.id.tvEmail_FNO)
    TextInputEditText emailView;
    @ViewById(R.id.tvVehicleNumber_FNO)
    TextInputEditText vehicleNumberView;
    @ViewById(R.id.llCustomerInfo_FNO)
    LinearLayout customerInfoView;
    @ViewById(R.id.tvOrderTotal_FNO)
    TextView totalView;
    @ViewById(R.id.tvPayStatus_FNO)
    TextView payStatusView;
    @ViewById(R.id.btnSelectCustomer_FNO)
    Button selectCustomerButton;
    @ViewById(R.id.btnPay_FNO)
    Button payButton;

    @FragmentArg
    OrderHeader orderHeader;

    @Bean
    protected OrderDetailsInteractor interactor;

    @AfterInject
    @Override
    public void initPresenter() {
        new OrderDetailsPresenter(this, interactor);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(R.string.title_new_order);
        startDateView.setText(DateManager.convertOrderDate(orderHeader.getStartDate()));
        endDateView.setText(DateManager.convertOrderDate(orderHeader.getEndDate()));
        categoryView.setText(orderHeader.getPlaceItem().getPlaceCategory().getName());
        sectorView.setText(orderHeader.getPlaceItem().getName());
        totalView.setText(String.valueOf(orderHeader.getTotal()));
        if (presenter == null)
            initPresenter();
        presenter.subscribe();
        presenter.updateOrderInfo(orderHeader);
        customerInfoView.setVisibility(View.GONE);
    }

    @Click(R.id.btnPay_FNO)
    void onSaveClick() {
        orderHeader.setCustomer(getCustomer());
        presenter.onSaveOrderClick(orderHeader);
    }

    @Click(R.id.btnCancel_FNO)
    void onCancelClick() {
        showHomeScreen();
    }

    @Click(R.id.btnSelectCustomer_FNO)
    void onSelectCustomerClick() {
        FindCustomerFragment fragment = FindCustomerFragment_.builder()
                .build();
        fragment.setTargetFragment(this, Constants.CUSTOMER_SELECTED);
        activity.replaceFragment(fragment);
    }

    @OnActivityResult(Constants.CUSTOMER_SELECTED)
    protected void onCustomerSelected(int resultCode, @OnActivityResult.Extra(Constants.TAG_CUSTOMER) Customer customer) {
        if (resultCode == Activity.RESULT_OK) {
            this.customer = customer;
        }
    }

    @Override
    public String getScreenName() {
        return "NewOrderFragment";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_order_details;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showHomeScreen() {
        activity.replaceFragmentClearBackstack(HomeFragment_.builder().build());
    }

    @Override
    public void setCategory(String category) {
        categoryView.setText(category);
    }

    @Override
    public void setupOrderInfo() {
        customerInfoView.setVisibility(customer == null ? View.GONE : View.VISIBLE);
        payButton.setEnabled(customer != null && orderHeader.getStatus() != com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsContract.ORDER_PAID);
        selectCustomerButton.setEnabled(orderHeader.getStatus() != com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsContract.ORDER_PAID);
        customerInfoView.setEnabled(orderHeader.getStatus() != com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsContract.ORDER_PAID);
        if (orderHeader.getStatus() == com.peter_ombodi.boomerang.android.presentation.screen.main.order.order_details.OrderDetailsContract.ORDER_PAID)
            payStatusView.setText(R.string.order_status_payed);
        if (customer != null) {
            firstNameView.setText(customer.getFirstName());
            lastNameView.setText(customer.getLastName());
            phoneView.setText(customer.getPhone());
            emailView.setText(customer.getEmail());
            vehicleNumberView.setText(customer.getVehicleNumber());
        }
    }

    @Override
    public void showBill(OrderHeader orderHeader) {
//        View v1 = getActivity().findViewById(R.id.cv_FOD);
//        takeScreenShot(v1);
        ShareBillFragment fragment = ShareBillFragment_.builder()
                .orderHeader(orderHeader)
                .build();
        fragment.setTargetFragment(this, Constants.BILL_SHARED);
        activity.replaceFragment(fragment);
    }

    @Override
    public void onOrderInfoUpdated(OrderHeader orderHeader) {
        this.orderHeader = orderHeader;
        setupOrderInfo();
    }


    @OnActivityResult(Constants.BILL_SHARED)
    protected void onBillShared(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            showHomeScreen();
        }
        if (resultCode == Constants.RESULT_SAVE_ERROR) {
            Toast.makeText(getActivity(), getResources().getString(R.string.msg_storage_write_error), Toast.LENGTH_LONG).show();
            showHomeScreen();
        }
    }

    private Customer getCustomer() {
        customer.setFirstName(firstNameView.getText().toString());
        customer.setLastName(lastNameView.getText().toString());
        customer.setPhone(phoneView.getText().toString());
        customer.setVehicleNumber(vehicleNumberView.getText().toString());
        customer.setEmail(emailView.getText().toString());
        return customer;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        setupOrderInfo();
    }

    private void takeScreenShot(View view){
        String mPath = Environment.getExternalStorageDirectory().toString() + "/" + "screen.jpg";
        Bitmap bitmap;
        view.setDrawingCacheEnabled(true);
        bitmap = Helpers.loadBitmapFromView(view, view.getWidth(), view.getHeight());
        view.setDrawingCacheEnabled(false);
        OutputStream outputStream = null;
        File imageFile = new File(mPath);
        try {
            outputStream = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
