package com.peter_ombodi.boomerang.android.presentation.screen.main.settings;

import android.text.InputType;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.settings.BillSettings;
import com.peter_ombodi.boomerang.android.data.setting.SettingsManager;
import com.peter_ombodi.boomerang.android.presentation.base.IBasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.CustomSpinner;
import com.peter_ombodi.boomerang.android.presentation.custom_view.spinner.quick_action_menu.ActionItem;
import com.peter_ombodi.boomerang.android.presentation.screen.main.home.HomeFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;
import com.peter_ombodi.boomerang.android.presentation.utils.LocaleUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Ombodi (Created on 11.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends MVPFragment<SettingsContract.SettingsPresenter> implements SettingsContract.SettingsView {

    @Bean
    SettingsManager settingsManager;

    @Bean
    protected SettingsInteractor interactor;

    @StringRes(R.string.title_settings)
    protected String toolbarTitle;

    @ViewById(R.id.tvTitle_FS)
    TextView titleView;
    @ViewById(R.id.tvSubtitle_FS)
    TextView subtitleView;
    @ViewById(R.id.tvAddress_FS)
    TextView addressView;
    @ViewById(R.id.tvPhone_FS)
    TextView phoneView;
    @ViewById(R.id.tvEmail_FS)
    TextView emailView;
    @ViewById(R.id.tvSlogan_FS)
    TextView sloganView;
    @ViewById(R.id.spLocale_FS)
    CustomSpinner localeSpinner;

    @AfterInject
    @Override
    public void initPresenter() {
        new SettingsPresenter(this, interactor);
    }

    @AfterViews
    protected void initSettingsView() {
        activity.getToolbarManager().setTitle(toolbarTitle);
        BillSettings billSettings = settingsManager.getBillSettings();
        titleView.setText(billSettings.getBillTitle());
        subtitleView.setText(billSettings.getBillSubtitle());
        addressView.setText(billSettings.getAddress());
        phoneView.setText(billSettings.getPhone());
        sloganView.setText(billSettings.getSlogan());
        emailView.setText(billSettings.getEmail());
        localeSpinner.setInputType(InputType.TYPE_NULL);
        localeSpinner.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                hideKeyboard();
                localeSpinner.callOnClick();
            }
        });

        setCategories();
    }

    @Click(R.id.btnSave_FS)
    void onSaveClick() {
        BillSettings billSettings = new BillSettings();
        billSettings.setBillTitle(titleView.getText().toString());
        billSettings.setBillSubtitle(subtitleView.getText().toString());
        billSettings.setAddress(addressView.getText().toString());
        billSettings.setPhone(phoneView.getText().toString());
        billSettings.setEmail(emailView.getText().toString());
        billSettings.setSlogan(sloganView.getText().toString());
        settingsManager.saveBillSettings(billSettings);
        switch (localeSpinner.getIdItem()){
            case 0:
                settingsManager.saveLocaleLanguage(Constants.LOCALE_LANG_UK);
                settingsManager.saveLocaleRegion(Constants.LOCALE_REGION_UK);
                break;
            case 1:
                settingsManager.saveLocaleLanguage(Constants.LOCALE_LANG_UA);
                settingsManager.saveLocaleRegion(Constants.LOCALE_REGION_UA);
                break;
            case 2:
                settingsManager.saveLocaleLanguage(Constants.LOCALE_LANG_HU);
                settingsManager.saveLocaleRegion(Constants.LOCALE_REGION_HU);
                break;
        }
        LocaleUtils.setAppLocale(getActivity(), settingsManager.getLocaleLanguage());
        getActivity().recreate();
        showHomeScreen();
    }

    @Click(R.id.btnCancel_FS)
    void onCancelClick() {
        showHomeScreen();
    }


    @Override
    public String getScreenName() {
        return "Settings Screen";
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_settings;
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    private void showHomeScreen() {
        hideKeyboard();
        activity.replaceFragmentClearBackstack(HomeFragment_.builder().build());
    }

    public void setCategories() {
        List<ActionItem> spinnerItems = new ArrayList<>();

        spinnerItems.add(new ActionItem(0, "English", R.drawable.ic_flag_gb_24));
        spinnerItems.add(new ActionItem(1, "Українська", R.drawable.ic_flag_ua_24));
        spinnerItems.add(new ActionItem(2, "Magyar", R.drawable.ic_flag_hu_24,0,false));
        localeSpinner.setDropDownItems(spinnerItems);

        switch (settingsManager.getLocaleLanguage()){
            case Constants.LOCALE_LANG_UK:
                localeSpinner.setItem(0);
                break;
            case Constants.LOCALE_LANG_UA:
                localeSpinner.setItem(1);
                break;
            case Constants.LOCALE_LANG_HU:
                localeSpinner.setItem(2);
                break;
        }
    }

}
