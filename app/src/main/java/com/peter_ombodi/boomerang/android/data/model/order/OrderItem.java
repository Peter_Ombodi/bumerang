package com.peter_ombodi.boomerang.android.data.model.order;

import com.google.gson.annotations.SerializedName;
import com.peter_ombodi.boomerang.android.data.model.user.AppUser;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class OrderItem implements Serializable{

    private Integer id;

    @SerializedName("add_time")
    private long addTime;

    @SerializedName("edit_time")
    private long editTime;

    private Integer status;

    @SerializedName("add_user")
    private AppUser addUser;

    @SerializedName("edit_user")
    private AppUser editUser;

    private Integer price;

    private Integer quantity;
}
