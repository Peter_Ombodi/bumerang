package com.peter_ombodi.boomerang.android.presentation.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.AsyncTaskLoader;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author Peter Ombodi (Created on 25.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class BitmapCompress extends AsyncTaskLoader<File> {

    public static final String ARGS_BITMAP = "ARGS_BITMAP";
    public static final String FILE_NAME = "FILE_NAME";
    private static final String TAG = "BitmapCompress";
    private Bitmap bitmap;

    private File file;
    private String fileName;
    private boolean success;

    public BitmapCompress(Context context, Bundle args) {
        super(context);
        if (args != null) {
            bitmap = args.getParcelable(ARGS_BITMAP);
            fileName = args.getString(FILE_NAME);
        }
    }

    @Override
    protected void onStartLoading() {
        if (file==null){
            forceLoad();
        } else {
            deliverResult(file);
        }
        super.onStartLoading();
    }

    @Override
    public File loadInBackground() {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + fileName);
        try {
            success = bitmap.compress(Bitmap.CompressFormat.PNG, 90, new FileOutputStream(file));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }
}
