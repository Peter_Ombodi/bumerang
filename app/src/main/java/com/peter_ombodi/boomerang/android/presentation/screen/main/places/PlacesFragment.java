package com.peter_ombodi.boomerang.android.presentation.screen.main.places;

import android.view.MenuItem;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;
import com.peter_ombodi.boomerang.android.presentation.base.tabs.MVPTabsFragment;
import com.peter_ombodi.boomerang.android.presentation.base.tabs.TabPagerAdapter;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.CategoriesFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details.CategoryDetailsFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.category_details.CategoryDetailsFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.SectorsFragment_;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details.SectorDetailsFragment;
import com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details.SectorDetailsFragment_;
import com.peter_ombodi.boomerang.android.presentation.utils.Constants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.res.StringRes;


/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment(R.layout.fragment_places_tabs_container)
@OptionsMenu(R.menu.menu_add_item)
public class PlacesFragment extends MVPTabsFragment {

    public static final int CATEGORIES_TAB = 0;
    public static final int SECTORS_TAB = 1;


    @FragmentArg
    protected Integer defaultTab;

    @StringRes(R.string.title_tab_categories)
    protected String titleTabCategories;
    @StringRes(R.string.title_tab_sectors)
    protected String titleTabSectors;
    @StringRes(R.string.title_places)
    protected String toolbarTitle;

    @OptionsMenuItem(R.id.menuAddItem)
    protected MenuItem menuAddItem;

    @AfterInject
    protected void initDefaultTab() {
        onPageChangeListener.onPageSelected(defaultTab != null ? defaultTab : 0);
    }

    @AfterViews
    protected void initViews() {
        activity.getToolbarManager().setTitle(toolbarTitle);
        vpContent_FCT.setOffscreenPageLimit(1);
        vpContent_FCT.postDelayed(() -> {
            vpContent_FCT.setCurrentItem(defaultTab != null ? defaultTab : 0);
        }, 10);
    }

    @Override
    public void addFragmentsToAdapter(TabPagerAdapter adapter) {
        adapter.addFragment(CategoriesFragment_.builder().build(), titleTabCategories);
        adapter.addFragment(SectorsFragment_.builder().build(), titleTabSectors);
    }

    @OptionsItem(R.id.menuAddItem)
    void onAddItemClick() {
        switch (vpContent_FCT.getCurrentItem()) {
            case CATEGORIES_TAB:
                addCategory();
                break;
            case SECTORS_TAB:
                addSector();
                break;
        }
    }

    private void addCategory() {
        //defaultTab = CATEGORIES_TAB;
        CategoryDetailsFragment fragment = CategoryDetailsFragment_.builder()
                .build();
        fragment.setTargetFragment(this, Constants.CATEGORY_SAVED);
        activity.replaceFragment(fragment);
    }

    private void addSector() {
        //defaultTab = SECTORS_TAB;
        SectorDetailsFragment fragment = SectorDetailsFragment_.builder()
                .build();
        fragment.setTargetFragment(this, Constants.SECTOR_SAVED);
        activity.replaceFragment(fragment);
    }

//    @OnActivityResult(Constants.SECTOR_SAVED)
//    protected void onSectorSaved(int resultCode) {
//        if (resultCode == Activity.RESULT_OK) {
//            defaultTab = SECTORS_TAB;
//        }
//    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_places_tabs_container;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void initPresenter() {
    }

    @Override
    public String getScreenName() {
        return "PlacesFragment";
    }
}
