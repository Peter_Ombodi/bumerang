package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */

public final class CategoriesPresenter extends BasePresenter implements CategoriesContract.CategoriesPresenter {

    private CategoriesContract.CategoriesView view;
    private CategoriesContract.CategoriesModel model;

    CategoriesPresenter(CategoriesContract.CategoriesView view, CategoriesContract.CategoriesModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void getCategories() {
        view.showProgressMain();
        compositeSubscriptions.add(
                model.getCategories().subscribe(categories -> {
                            view.showCategories(categories);
                            view.hideProgress();
                        },
                        throwable -> {
                            view.hideProgress();
                            throwable.printStackTrace();
                        }));
    }

    @Override
    public void onItemSelected(PlaceCategory placeCategory) {
        view.editCategory(placeCategory);
    }
}
