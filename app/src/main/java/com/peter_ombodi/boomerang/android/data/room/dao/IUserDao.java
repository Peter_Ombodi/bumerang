package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peter_ombodi.boomerang.android.data.room.tables.TUser;

import java.util.List;

import io.reactivex.Single;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Dao
public interface IUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TUser... users);

    @Query("DELETE FROM users")
    void nukeTable();

    @Query("SELECT * FROM users WHERE id = :userIds LIMIT 1")
    TUser getUserById(String userIds);

    @Query("SELECT * FROM users WHERE is_active = 1")
    List<TUser> getAllActiveUsers();

    @Query("SELECT * FROM users")
    List<TUser> getAllUsers();

    @Query("SELECT * FROM users WHERE id IN (:userIds)")
    Single<List<TUser>> getAllByIds(String[] userIds);

}
