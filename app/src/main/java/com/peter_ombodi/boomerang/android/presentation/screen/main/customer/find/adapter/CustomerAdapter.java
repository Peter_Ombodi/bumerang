package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class CustomerAdapter extends RecyclerView.Adapter<CustomerVH> implements IProviderItemClickListener<Customer> {

    private IItemClickListener<Customer> clickListener;
    private List<Customer> list;

    public void setData(List<Customer> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        if (list != null) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    public void notifySlot(Customer item) {
        notifyItemChanged(list.indexOf(item));
    }

    @NonNull
    @Override
    public CustomerVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomerVH vh = new CustomerVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_customer, parent, false));
        vh.setOnItemClickListener(clickListener);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull CustomerVH holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<Customer> listener) {
        this.clickListener = listener;
    }
}
