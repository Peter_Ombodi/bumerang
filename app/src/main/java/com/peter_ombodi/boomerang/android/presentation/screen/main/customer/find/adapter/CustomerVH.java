package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class CustomerVH extends RecyclerView.ViewHolder implements IProviderItemClickListener<Customer> {

    private TextView nameView;
    private TextView phoneView;
    private TextView vehicleNumberView;
    private TextView emailView;

    public CustomerVH(View itemView) {
        super(itemView);
        nameView = itemView.findViewById(R.id.tvName_VIC);
        phoneView = itemView.findViewById(R.id.tvPhone_VIC);
        vehicleNumberView = itemView.findViewById(R.id.tvVehicle_VIC);
        emailView = itemView.findViewById(R.id.tvEmail_VIC);
    }

    public void bind(Customer item) {
        itemView.setTag(item);
        nameView.setText(String.format("%s %s", item.getFirstName(), item.getLastName()));
        phoneView.setText(item.getPhone());
        vehicleNumberView.setText(item.getVehicleNumber());
        emailView.setText(item.getEmail());
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<Customer> listener) {
        itemView.setOnClickListener(view -> listener.onClick(view, (Customer) view.getTag()));
    }
}
