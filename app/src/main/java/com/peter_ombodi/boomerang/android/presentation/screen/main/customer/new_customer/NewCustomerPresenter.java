package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.new_customer;

import android.util.Log;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class NewCustomerPresenter extends BasePresenter implements NewCustomerContract.NewCustomerPresenter {

    private NewCustomerContract.NewCustomerView view;
    private NewCustomerContract.NewCustomerModel model;

    public NewCustomerPresenter(NewCustomerContract.NewCustomerView view, NewCustomerContract.NewCustomerModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void onSaveClick(Customer customer) {
        compositeSubscriptions.add(
                model.saveCustomer(customer)
                        .subscribe(newId->{
                            Log.d("NewCustomerPresenter", "onSaveClick: Ok");;
                            customer.setId(newId);
                            view.onCustomerSaved(customer);
                        },Throwable::printStackTrace));
    }
}
