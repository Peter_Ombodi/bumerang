package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.sector_details;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceCategoryDao;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlace;
import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 09.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class SectorDetailsInteractor extends BaseInteractor implements SectorDetailsContract.SectorModel {

    @Bean
    RoomBase roomBase;

    private IPlaceDao sectorDao;
    private IPlaceCategoryDao placeCategoryDao;

    @AfterInject
    void init() {
        sectorDao = roomBase.getPlaceDao();
        placeCategoryDao = roomBase.getPlaceCategoryDao();
    }

    @Override
    public Observable<Long> saveSector(PlaceItem item) {
        return getAsyncObservable(Observable.fromCallable(()
                -> sectorDao.insertPlace(TPlace.fromPlace(item))));
    }

    @Override
    public Observable<Integer> deleteSector(long sectorId) {
        return getAsyncObservable(Observable.fromCallable(()
                -> sectorDao.deletePlace(sectorId)));
    }

    @Override
    public Observable<List<PlaceCategory>> getCategories() {
        return getAsyncObservable(placeCategoryDao.getAllPlaceCategories().toObservable()
                .map(tPlaceCategories -> {
                            List<PlaceCategory> responses = new ArrayList<>();
                            for (TPlaceCategory category : tPlaceCategories)
                                responses.add(category.toPlaceCategory());
                            return responses;
                        }
                ));
    }
}