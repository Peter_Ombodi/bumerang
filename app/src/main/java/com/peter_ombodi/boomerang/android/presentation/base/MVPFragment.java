package com.peter_ombodi.boomerang.android.presentation.base;

import android.content.Context;

import com.peter_ombodi.boomerang.android.presentation.base.content.ContentFragment;
import com.peter_ombodi.boomerang.android.presentation.utils.KeyboardManager;

import org.androidannotations.annotations.EFragment;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EFragment
public abstract class MVPFragment <T extends IBasePresenter> extends ContentFragment implements IBaseContentView<T> {

    protected T presenter;
    protected MVPActivity activity;

    @Override
    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MVPActivity) context;
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null){
            presenter.unsubscribe();
            presenter = null;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        KeyboardManager.hideKeyboard(getActivity());
    }
}
