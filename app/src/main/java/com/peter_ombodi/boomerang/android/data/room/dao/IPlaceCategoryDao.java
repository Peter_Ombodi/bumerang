package com.peter_ombodi.boomerang.android.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peter_ombodi.boomerang.android.data.room.tables.TPlaceCategory;

import java.util.List;

import io.reactivex.Maybe;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@Dao
public interface IPlaceCategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TPlaceCategory... placeCategories);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TPlaceCategory> placeCategories);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCategory(TPlaceCategory placeCategories);

    @Query("DELETE FROM place_categories where id=:categoryId")
    int deleteCategory(int categoryId);

    @Query("DELETE FROM place_categories")
    void nukeTable();

    @Query("SELECT * FROM place_categories WHERE `id` = :categoryId LIMIT 1")
    TPlaceCategory getPlaceCategoryById(String categoryId);

    @Query("SELECT * FROM place_categories order by default_price, name")
    Maybe<List<TPlaceCategory>> getAllPlaceCategories();

}
