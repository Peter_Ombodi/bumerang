package com.peter_ombodi.boomerang.android.presentation.screen.main.home;

import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

/**
 * @author Peter Ombodi (Created on 01.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public final class HomePresenter extends BasePresenter implements HomeContract.HomePresenter {

    private HomeContract.HomeView view;
    private HomeContract.HomeModel model;

    public HomePresenter(HomeContract.HomeView view, HomeContract.HomeModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }
}
