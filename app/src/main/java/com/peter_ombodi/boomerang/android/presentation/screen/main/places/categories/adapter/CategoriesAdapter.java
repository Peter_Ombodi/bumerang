package com.peter_ombodi.boomerang.android.presentation.screen.main.places.categories.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceCategory;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceItem;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class CategoriesAdapter extends RecyclerView.Adapter<CategoryVH> implements IProviderItemClickListener<PlaceCategory> {

    private IItemClickListener<PlaceCategory> clickListener;
    private List<PlaceCategory> items;

    public void setData(List<PlaceCategory> list) {
        items = list;
        notifyDataSetChanged();
    }

    public void clear() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }

    public void notifySlot(PlaceItem item) {
        notifyItemChanged(items.indexOf(item));
    }

    @NonNull
    @Override
    public CategoryVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CategoryVH vh = new CategoryVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_category, parent, false));
        vh.setOnItemClickListener(clickListener);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryVH holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public void setOnItemClickListener(IItemClickListener<PlaceCategory> listener) {
        this.clickListener = listener;
    }
}