package com.peter_ombodi.boomerang.android.presentation.screen.main.customer.find;

import com.peter_ombodi.boomerang.android.data.model.user.Customer;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.ICustomerDao;
import com.peter_ombodi.boomerang.android.data.room.tables.TCustomer;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 16.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class FindCustomerInteractor extends BaseInteractor implements FindCustomerContract.FindCustomerModel {

    @Bean
    RoomBase roomBase;

    private ICustomerDao customerDao;

    @AfterInject
    void init() {
        customerDao = roomBase.getCustomerDao();
    }

    @Override
    public Observable<List<Customer>> getAllCustomers() {
        return getAsyncObservable(customerDao.getAllCustomers().toObservable().map(tCustomers -> {
            List<Customer> customers = new ArrayList<>();
            for (TCustomer tCustomer : tCustomers) {
                customers.add(TCustomer.fromTCustomer(tCustomer));
            }
            return customers;
        }));
    }
}
