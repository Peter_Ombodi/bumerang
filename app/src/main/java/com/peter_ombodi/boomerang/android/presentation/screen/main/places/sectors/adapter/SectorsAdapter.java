package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.peter_ombodi.boomerang.android.R;
import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.presentation.base.IItemClickListener;
import com.peter_ombodi.boomerang.android.presentation.base.IProviderItemClickListener;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */
    @EBean
    public class SectorsAdapter extends RecyclerView.Adapter<SectorVH> implements IProviderItemClickListener<PlaceFlat> {

        private IItemClickListener<PlaceFlat> clickListener;
        private List<PlaceFlat> PlaceFlats;

        public void setData(List<PlaceFlat> list) {
            PlaceFlats = list;
            notifyDataSetChanged();
        }

        public void clear() {
            if (PlaceFlats != null) {
                PlaceFlats.clear();
                notifyDataSetChanged();
            }
        }

        public void notifySlot(PlaceFlat item) {
            notifyItemChanged(PlaceFlats.indexOf(item));
        }

        @NonNull
        @Override
        public SectorVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            SectorVH vh = new SectorVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_sector, parent, false));
            vh.setOnItemClickListener(clickListener);
            return vh;

        }

        @Override
        public void onBindViewHolder(@NonNull SectorVH holder, int position) {
            holder.bind(PlaceFlats.get(position));
        }

        @Override
        public int getItemCount() {
            return PlaceFlats == null ? 0 : PlaceFlats.size();
        }

        @Override
        public void setOnItemClickListener(IItemClickListener<PlaceFlat> listener) {
            this.clickListener = listener;
        }
    }
