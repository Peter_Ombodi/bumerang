package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors;

import android.text.TextUtils;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */
public final class SectorsPresenter extends BasePresenter implements SectorsContract.SectorsPresenter {

    private SectorsContract.SectorsView view;
    private SectorsContract.SectorsModel model;
    private List<PlaceFlat> fullSectorList;

    public SectorsPresenter(SectorsContract.SectorsView view, SectorsContract.SectorsModel model) {
        this.view = view;
        this.model = model;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        this.view = null;
        this.model = null;
    }

    @Override
    public void getSectors() {
        view.showProgressMain();
        compositeSubscriptions.add(
                model.getSectors().subscribe(sectors -> {
                            view.showSectors(sectors);
                            fullSectorList = sectors;
                            view.hideProgress();
                        },
                        throwable -> {
                            view.hideProgress();
                            throwable.printStackTrace();
                        }));
    }

    @Override
    public void onItemSelected(PlaceFlat placeItem) {
        view.editSector(placeItem.toPlaceItem());
    }

    @Override
    public Observable<List<PlaceFlat>> filterSectors(String filter) {
        return Observable.fromCallable(() -> {
            if (!TextUtils.isEmpty(filter)) {
                return filterOrders(fullSectorList, filter);
            } else return fullSectorList;
        });
    }

    private List<PlaceFlat> filterOrders(List<PlaceFlat> fullList, String query) {
        final List<PlaceFlat> list = new ArrayList<>();
        for (PlaceFlat item : fullList) {
            if (containsQuery(item.getName(), query) ||
                    containsQuery(item.getCategoryName(), query) ||
                    containsQuery(String.valueOf(item.getPrice()), query)) {
                list.add(item);
            }
        }
        return list;
    }

    private boolean containsQuery(String field, String query) {
        return (!TextUtils.isEmpty(field) && field.toLowerCase().contains(query.toLowerCase()));
    }
}