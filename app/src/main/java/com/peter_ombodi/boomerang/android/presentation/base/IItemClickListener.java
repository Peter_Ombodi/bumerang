package com.peter_ombodi.boomerang.android.presentation.base;

import android.view.View;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IItemClickListener <T> {
    void onClick(View view, T model);
}
