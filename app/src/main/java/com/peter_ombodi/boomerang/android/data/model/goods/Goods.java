package com.peter_ombodi.boomerang.android.data.model.goods;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Peter Ombodi (Created on 05.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class Goods implements Serializable {

    private Integer id;

    @SerializedName("goods_category")
    private GoodsCategory goodsCategory;

    private String name;

    private Integer price;

    @SerializedName("is_favorite")
    private boolean isFavorite;

}
