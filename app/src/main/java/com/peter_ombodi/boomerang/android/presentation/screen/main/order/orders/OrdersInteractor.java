package com.peter_ombodi.boomerang.android.presentation.screen.main.order.orders;

import com.peter_ombodi.boomerang.android.data.model.order.OrderFlat;
import com.peter_ombodi.boomerang.android.data.model.order.OrderHeader;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IOrderDao;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 27.08.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class OrdersInteractor extends BaseInteractor implements OrdersContract.OrdersModel {

    @Bean
    RoomBase roomBase;

    private IOrderDao orderDao;

    @AfterInject
    void init() {
        orderDao = roomBase.getOrderDao();
    }

    @Override
    public Observable<List<OrderFlat>> getSoldPlaces(int orderFilter, String date) {
        switch (orderFilter) {
            case OrdersContract.ORDERS_ALL:
                return getAsyncObservable(orderDao.getSoldPlaces().toObservable());
            case OrdersContract.ORDERS_ARRIVE:
                return getAsyncObservable(orderDao.getArrivePlaces(date).toObservable());
            case OrdersContract.ORDERS_LEAVE:
                return getAsyncObservable(orderDao.getLeavePlaces(date).toObservable());
        }
        return null;
    }

    @Override
    public Observable<List<OrderHeader>> getTodayArrivePlaces() {
        return null;
    }

    @Override
    public Observable<List<OrderHeader>> getTodayLeavePlaces() {
        return null;
    }
}
