package com.peter_ombodi.boomerang.android.develop;

import android.app.Application;

import okhttp3.OkHttpClient;

/**
 * @author Peter Ombodi (Created on 04.07.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public interface IStethoConfigure {
    void init(final Application application);
    void configureInterceptor(OkHttpClient.Builder httpClient);
}
