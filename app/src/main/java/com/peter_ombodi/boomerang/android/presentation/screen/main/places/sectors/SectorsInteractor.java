package com.peter_ombodi.boomerang.android.presentation.screen.main.places.sectors;

import com.peter_ombodi.boomerang.android.data.model.place.PlaceFlat;
import com.peter_ombodi.boomerang.android.data.room.RoomBase;
import com.peter_ombodi.boomerang.android.data.room.dao.IPlaceDao;
import com.peter_ombodi.boomerang.android.presentation.base.BaseInteractor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Peter Ombodi (Created on 02.09.2018).
 * Email:  p.ombodi@gmail.com
 */
@EBean
public class SectorsInteractor extends BaseInteractor implements SectorsContract.SectorsModel {

    @Bean
    RoomBase roomBase;

    private IPlaceDao placeDao;


    @AfterInject
    void init() {
        placeDao = roomBase.getPlaceDao();
    }

    @Override
    public Observable<List<PlaceFlat>> getSectors() {
        return getAsyncObservable(placeDao.getAllPlaces().toObservable());
    }
}
