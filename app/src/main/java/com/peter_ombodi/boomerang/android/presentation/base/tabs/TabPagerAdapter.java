package com.peter_ombodi.boomerang.android.presentation.base.tabs;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.peter_ombodi.boomerang.android.presentation.base.MVPFragment;

import java.util.ArrayList;

/**
 * @author Peter Ombodi (Created on 01.09.2018).
 * Company: Peter Ombodi
 * Email:  p.ombodi@gmail.com
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> titles;
    private ArrayList<MVPFragment> fragments;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);

        fragments = new ArrayList<>();
        titles = new ArrayList<>();
    }

    public void addFragment(MVPFragment fragment, String title) {
        fragments.add(fragment);
        titles.add(title);
        notifyDataSetChanged();
    }

    @Override
    public MVPFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
